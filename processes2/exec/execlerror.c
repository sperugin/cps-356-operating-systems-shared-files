#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main() {
   //execl("/bin/ls", "lsllll", "-z", NULL);
   execlp("rrty", "lsllll", "-z", NULL);
   perror("Child failed to exec ls");
   return 1; 
}
