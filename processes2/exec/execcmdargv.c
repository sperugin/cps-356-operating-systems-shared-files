/* execcmdargv.c: ref. [USP] Chapter 3, Program 3.6, p. 82 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
//#include "restart.h"

int main(int argc, char* argv[]) {
   pid_t childpid;
   char delim[] = " \t";
   char** myargv = NULL;

   if (argc != 2) {
      fprintf(stderr, "Usage: %s string\n", argv[0]);
      return 1; 
   }

   if ((childpid = fork()) == -1) {
      perror("Failed to fork");
      return 1; 
   }

   if (childpid == 0) {                              /* child code */
     if (buildnewargv(argv[1], delim, &myargv) == -1) {
        perror("Child failed to construct argument array");
     } else {
        execvp(myargv[0], &myargv[0]);
        perror("Child failed to exec command");
     }   
     return 1;
   }
   if (childpid != wait(NULL)) {                  /* parent code */
      perror("Parent failed to wait");
      return 1;
   }
   return 0; 
}
