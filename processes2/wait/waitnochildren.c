#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main (int argc, char** argv) {
   pid_t x = wait(NULL);

   fprintf(stderr, "%ld\n", (long) x);
}
