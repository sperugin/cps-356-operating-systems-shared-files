/* fanwait.c: ref. [USP] Chapter 3, Exercise 3.15, p. 73 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
//#include "restart.h"

int main (int argc, char** argv) {
   pid_t childpid;
   int i, n;

   if (argc != 2) {
      fprintf(stderr, "Usage: %s n\n", argv[0]);
      return 1;
   }

   n = atoi(argv[1]);

   for (i = 1; i < n; i++)
      if ((childpid = fork()) <= 0) {
         break;
      }

   /* wait for all of your children */
   while (wait(NULL) > 0);

   fprintf(stderr, "i:%d  process ID:%ld  parent ID:%ld  child ID:%ld\n",
           i, (long) getpid(), (long) getppid(), (long) childpid);

   //sleep(2);
   return 0;
}
