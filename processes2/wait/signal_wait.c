#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/wait.h>

#define SECONDS_TO_SLEEP 30

int main() {

   int status;

   if (fork() == 0)
         /* child code */
        sleep(SECONDS_TO_SLEEP);

   else  {
           //wait(NULL);
           wait(&status); 
           if (WIFSIGNALED(status))
              fprintf(stderr, "My child was terminated by signal.\n");
           else
              fprintf(stderr, "My child terminated normally.\n");

        }
}
