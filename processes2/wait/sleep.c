#include<stdio.h>
#include<unistd.h>
#define SECONDS_TO_SLEEP 30

main() {
   fprintf (stderr, "I am proces %ld, and am sleeping for %d seconds.\n",
           getpid(), SECONDS_TO_SLEEP);
   sleep(SECONDS_TO_SLEEP);
}
