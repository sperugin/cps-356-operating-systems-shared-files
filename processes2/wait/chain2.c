/* chainwaitmsg.c: ref. [USP] Chapter 3, Exercise 3.21, p. 76 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main (int argc, char** argv) {
   pid_t childpid = 0; 
   int i = 1;
   int n;
   int status;
 
   /* check for valid number of command-line arguments */ 
   if (argc != 2) {
      fprintf(stderr, "Usage: %s processes\n", argv[0]);
      return 1; 
   }     

   n = atoi(argv[1]);  

   for (i = 1; i < n; i++) {
      if (childpid = fork()) {
         //wait(NULL);
         wait(&status);
         fprintf(stderr, "i:%d  process ID:%ld  parent ID:%ld  child ID:%ld\n", i, (long)getpid(), (long)getppid(), (long)childpid);
         fprintf(stderr, "My child exited with status %d.\n", WEXITSTATUS(status));
         break;
      } 
   }

   if (i == n) {
      fprintf(stderr, "i:%d  process ID:%ld  parent ID:%ld  child ID:%ld\n",
      i, (long)getpid(), (long)getppid(), (long)childpid);
   }

   exit(i);
}
