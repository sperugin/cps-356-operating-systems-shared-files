int main() {

   /* this literal array of characters (string) is placed
      the read-only section of the process. */

   char* s = "hello world";

   *s = 'H';
}
