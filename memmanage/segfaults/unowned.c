#include<stdio.h>

/* OS is responsible for protecting a process' memory space. */

main() {

   int x = 10;
   int* intptr = &x;

   printf ("%x\n", intptr);
   printf ("%x\n", (intptr+1000000000000));
   printf ("%d\n", *(intptr+1000000000000));
}
