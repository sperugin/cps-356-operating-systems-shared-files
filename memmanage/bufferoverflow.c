/* ref. [OSIDP] Fig. 7.13(a) on p. 332 */

#include<stdio.h>
#include<string.h>
#define FALSE 0
#define TRUE 1

main() {
   int valid = FALSE;
   char str1[16];
   char str2[16];

   strcpy(str1, "START");

   //gets(str2);
   fgets(str2,16,stdin);

   //scanf ("%15s", str2);

   //str2[strlen(str2)-1] = '\0';

   //printf ("valid(%d)\n", valid);
   //if (strncmp(str1, str2, 15) == 0)
   if (strcmp(str1, str2) == 0)
      valid = TRUE;
   //printf ("valid(%d)\n", valid);

   //printf ("valid(%x)", &valid);
   printf ("str1(%x)", str1);
   printf ("str2(%x)", str2);
   printf ("\n\n");

   printf ("buffer1: str1(%s), str2(%s), valid(%d)\n", str1, str2, valid);
}
