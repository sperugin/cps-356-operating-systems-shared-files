/*
 * address.c
 * 11Dec14
 * Joey Perme
 * 8.28, OSCJ8
 *
 * Paraphrased description:
 *    Assuming 32-bit addresses, write a program that is passed (1)
 *    the size of a page and (2) the virtual address. Your program
 *    will report the page number and offset of the givn virtual 
 *    address with the specified page size.
 *
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define MAX_CANNON 8192
#define ERR stderr

int main(int argc, char** argv){

    //if 1, will read from args
    //else will prompt user
    int arg_entry = 1;

    int page_size, virtual_address, page_number, offset;

    if(arg_entry){
        if(argc > 1){
            page_size = atoi(argv[1]);
            virtual_address = atoi(argv[2]);
            if(page_size%2){
                fprintf(ERR, "Page sizes must be given in powers of 2.\n");
                return(1);
            }
        } else {
            fprintf(ERR, "Syntax: address page_size virtual_address\n");
            return(1);
        }
    } else {
        fprintf(stdout, "Enter page size, pls: ");
        scanf("%d", &page_size);

        fprintf(stdout, "Enter virtual address, pls: ");
        scanf("%d", &virtual_address);
    }

    //page # = virtual_address/page_size
    page_number = (virtual_address/page_size);

    //offset = address - page_number*page_size
    offset = virtual_address - (page_number * page_size);

    fprintf(stdout, "The address %d contains:\n\n", virtual_address);
    fprintf(stdout, "page number: %d\n", page_number);
    fprintf(stdout, "offset: %d\n", offset);

    return(0);
}