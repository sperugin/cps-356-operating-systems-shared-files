/* simplefork.c: ref. [USP] Chapter 3, Example 3.5, p. 65 */

#include <stdio.h>
#include <unistd.h>

int main() {
   int x;

   x = 0;

   if (fork() > 0) {
      fprintf (stderr, "parent %x\n", &x);
      x = 1;
   } else {
      fprintf (stderr, "parent %x\n", &x);
     }
   fprintf (stderr, "I am process %ld and my parent is %ld. x = %d\n", (long) getpid(),
           (long) getppid(), x);
   return 0;
}
