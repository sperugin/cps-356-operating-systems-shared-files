import java.util.Scanner;

/**
 *
 * @author Black
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        long address = 0;
        String page, offset;
        
        //read input
        //Scanner in = new Scanner(System.in);
        if (args.length > 0) {
            try {
                address = Long.parseLong(args[0]);
                System.out.println("The address " + address + "contains:");
            } catch (NumberFormatException e) {
                System.err.println("Argument" + args[0] + " must be an address.");
                System.exit(1);
            }
        }
        
        page = Long.toBinaryString(address);
        
        //parse first 4 bits of binary string
        page = page.substring(0, 3);        
        //convert to decimal and output
        int pageDecimalValue = Integer.parseInt(page, 2);
        
        System.out.println("Page Number: " + pageDecimalValue);
        
        //now calculate offset
        offset = Long.toBinaryString(address).substring(3);
        int offsetDecimalValue = Integer.parseInt(offset, 2);
        
        System.out.println("Offset: " + offsetDecimalValue);
    }
    
}
