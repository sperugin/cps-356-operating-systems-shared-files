/*******************************************************************************
/
/      filename:  extra.c
/
/   description:  Takes the size of a page and the virtual address and gives
/                 the page number and offset.
/
/        author:  Hannan, Isolde
/      login id:  FA_14_CPS356_23
/
/         class:  CPS 356
/    instructor:  Perugini
/    assignment:  Extra Credit
/
/      assigned:  December 11, 2014
/           due:  December 15, 2014
/
/******************************************************************************/
#include <stdio.h>

int main(int argc, char*argv[])
{
   long unsigned long int pageSize = atoll(argv[1]);
   long unsigned long int address = atoll(argv[2]);
   long unsigned long int page = address;
   long unsigned long int offset = address;

   printf("The address %llu contains: \n", address);
   page = (int)address/pageSize;
   printf("Page number = %llu\n", page);
   offset = address%pageSize;
   printf("Offset = %llu\n", offset);
   return 0;
}
