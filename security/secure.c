//compiled with VS.NET
//courtesy William Kimball

#include <stdio.h>
#include <string.h>

//useful for never using functions that may be insecure
#define strcpy unsafe_strcpy
#define sprintf unsafe_sprint
#define strcat unsafe_strcat
#define gets unsafe_gets
#define scanf unsafe_scanf

int main(int argc, char **argv) {

   char dst[10];

   //incorrect
   //scanf("%s", dst);

   //incorrect
   //scanf("%9s", dst);

   //correct
   //scanf("%9s", dst);
   //fflush(stdin);

   //correct
   fgets(dst, sizeof(dst), stdin);

   printf("%s\n", dst);

   return 0;
}

/*int main(int argc, char **argv) {

   char src[] = "0123456789";
   char dst[10];

   //incorrect
   //sprintf(dst, "%s", src);

   //correct
   sprintf(dst, "%.*s", sizeof(dst)-1, src);

   printf("%s - %s", src, dst);

   return 0;
}*/

/*int main(int argc, char **argv) {

   char src[] = "0123456789";
   char dst[10];

   //incorrect
   /strcpy(dst, src);

   //correct
   strncpy(dst, src, sizeof(dst));
   dst[sizeof(dst)-1] = '\0';

   printf("%s - %s", src, dst);

   return 0;
}*/
