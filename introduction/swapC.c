#include<stdio.h>

/* swap call-by-value */
void swap_cbv (int x, int y) {
   int temp = x;
   x = y;
   y = temp;
}

/* swap function (call-by-reference) */
void swap_cbr (int* x, int* y) {
   int temp = *x;
   *x = *y;
   *y = temp;
}

main() {

   int a = 1;
   int b = 2;

   printf ("before call-by-value: a = %d, b = %d\n\n", a, b);
   swap_cbv (a, b);
   printf ("after call-by-value: a = %d, b = %d\n\n", a, b);
}
