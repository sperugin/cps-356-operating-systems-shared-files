/* author: Perugini */

#include<stdio.h>
#include<string.h>

main() {

   double number1 = 7.3578, number2;
   char s1[16];
   char s2[16];
   //char* s1 = "tom"; // strings declared this way are read-only
   //char* s2 = "cat";
   //char* s1 = strdup ("tom");
   //char* s2 = strdup ("cat");

   double* dPtr = &number1;

   s1[0] = 't';
   s1[1] = 'o';
   s1[2] = 'm';
   s1[3] = '\0';
   s2[0] = 'c';
   s2[1] = 'a';
   s2[2] = 't';
   s2[3] = '\0';

   printf ("%f\n", *dPtr);
   printf ("%10.2f\n", *dPtr);

   number2 = *dPtr;

   printf ("%3.2f\n", number2);
   printf ("%x\n", dPtr);
   printf ("%X\n", dPtr);
   printf ("%x\n", &number1);

   if (dPtr == &number1)
      printf("Yes, they are equal.\n");

   strcpy (s2,s1);

   printf ("result: %d\n", strcmp (s1,s2));
   strcat (s1,s2);
   printf (":%s:\n", s1);
   printf ("%d\n", strlen(s1));
}
