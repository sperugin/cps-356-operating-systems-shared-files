#include<stdio.h>

void swap(int* x, int* y) {
   int temp = *x;
   *x = *y;
   *y = temp;
}

main() {

   int a = 1;
   int b = 2;

   int* ptr = &a;
   int** ptr2ptr = &ptr;

   printf ("ptr is at %x.\n", &ptr);
   printf ("ptr2ptr is at %x.\n", &ptr2ptr);
   printf ("ptr2ptr points to %x.\n", *ptr2ptr);
   printf ("what ptr2ptr points to points to %x.\n", **ptr2ptr);

   printf ("a is at %x.\n", &a);
   printf ("b is at %x.\n", &b);

   swap(&a,&b);

   printf ("a is at %x.\n", &a);
   printf ("b is at %x.\n", &b);

   printf ("a = %d and b = %d.\n", a, b);
}
