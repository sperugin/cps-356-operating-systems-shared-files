/* author: Perugini */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

//struct {
struct part {
   int partNumber;
   char partName[25];
   //char* partName;
};

typedef struct part Part;

/*
typedef struct {
   int partNumber;
   char partName[26];
   //char* partName;
} Part;
*/

main() {

   Part a;
   struct part b[10];
   struct part* ptr = NULL;

   //a.partName = malloc(sizeof(*a.partName)*26);

   scanf ("%d", &a.partNumber);
   /* printf("this decimal is: %d", a.partNumber); */
   /* printf("this decimal is: %3d", a.partNumber); */
   //scanf ("%s", a.partName);
   scanf ("%25s", a.partName);

   //scanf ("%d%25s", &a.partNumber, a.partName);

   printf ("%d\n", a.partNumber);
   printf ("%s\n", a.partName);
   printf (":%s:\n", a.partName);

   b[3] = a;

/* same as
   b[3].partNumber = a.partNumber;
   strcpy(b[3].partName, a.partName);
   not b[3].partName = a.partName; which just copies pointer
 */

   //b[3].partName = malloc (sizeof(*b[3].partName)*26);

   strcpy (a.partName, "new");

   //printf ("%d\n", b[3].partNumber);
   //printf ("%3d\n", b[3].partNumber);

   b[3].partNumber = 156;
   b[3].partName[0] = ';';

   //a.partName[0] = 'a';

   printf ("%d\n", b[3].partNumber);
   printf ("%s\n", b[3].partName);

   printf ("%d\n", a.partNumber);
   printf ("%s\n", a.partName);

   ptr = b;

   printf ("%d\n", (ptr+3)->partNumber);
   printf ("%s\n", (ptr+3)->partName);

   printf("size of Part %d\n", sizeof(Part));
   printf("size of Part* %d\n", sizeof(Part*));
   printf ("%x\n", ptr);

/* here is what is happening:
   first we have to cast the ptr to a long so we
   can add 3 * the size of Part
   without doing pointer arithmetic; if we don't
   cast the route ptr to a long the compiler thinks
   we are adding 3 * the size of Part to a pointer (address) and does the
   translation for us;
 
   so once we cast ptr to a long, now we can add 3 * size of Part,
   but then inorder to use the -> operator to get the partNumber out,
   we need to cast that computed long back to a pointer to a Part;

   remember the lhs of the -> must always contain a ptr, not a long */
   printf("%d\n", ((Part*) (((long) (ptr))+(sizeof(Part)*3)))->partNumber);
   printf("%s\n", ((Part*) (((long) (ptr))+(sizeof(Part)*3)))->partName);

   printf("%d\n", (*(Part*) (((long) (ptr))+(sizeof(Part)*3))).partNumber);
   printf("%s\n", (*((Part*) (((long) (ptr))+(sizeof(Part)*3)))).partName);
}
