/* redirect.c: ref. [USP] Chapter 4, Program 4.18, p. 131 */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#define CREATE_FLAGS (O_WRONLY | O_CREAT | O_APPEND)
//#define CREATE_FLAGS (O_WRONLY | O_CREAT)
//#define CREATE_FLAGS (O_WRONLY | O_CREAT | O_TRUNC)
#define CREATE_MODE (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

int main () {

   //int mode = (O_WRONLY | O_CREAT | O_APPEND);

   //pid_t childpid = fork ();

   //FILE* fout = fopen ("testfile.txt", "a");
   //int fd = fileno(fout);
   int fd;

   int fdstdout = dup(STDOUT_FILENO);
   //int fdstdout = dup(1);

   if ((fd = open ("testfile.txt", CREATE_FLAGS, CREATE_MODE)) == -1) {
   //if ((fd = open ("testfile.txt", CREATE_FLAGS)) == -1) {
      perror ("Failed to open testfile.txt.");
      return 1;
   }

   //if (dup2 (fd, 1) == -1) {
   //if (dup2 (fileno(fout), STDOUT_FILENO) == -1) {
   if (dup2 (fd, STDOUT_FILENO) == -1) {
      perror ("Failed to redirect standard output.");
      return 1;
   }

   //fclose(fout);
   if (close (fd) == -1) {
      perror ("Failed to close the file.");
      return 1;
   }

   printf ("txstasadasdasd\n");
   printf("here\n");
   fflush (stdout);


   dup2 (fdstdout, STDOUT_FILENO);

   printf("print to stdout\n");
//sleep(100);
   if (write (STDOUT_FILENO, "Write to stdout this.", 21) == -1) {
      perror ("Failed in writing to file.");
      return 1;
   }
   //dup2 (fdstdout, STDOUT_FILENO);
   //printf("here\n");

   return 0;
}
