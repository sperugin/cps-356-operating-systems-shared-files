package main

import (
   "fmt"
   "time"
   "os"
   "strconv"
)

func producer(b chan<- string, i int) {
    var timestr string

//    for {
        timestr = time.Now().Local().Format("15:04:05.000")
        b<-timestr
        fmt.Fprintf(os.Stderr, "Producer %d just put %s in the shared buffer.\n", i, timestr)
 //   }
}


func consumer(t <-chan string, finish chan string, i int) {
 //  for {
      time.Sleep(time.Second*2) // time to consume
      select {
         case item := <-t:
            fmt.Fprintf(os.Stderr, "Consumer %d just consumed %s from the shared buffer.\n", i, item)
         default:
              finish <- "we're done!"
      }
 //  }
}

func main() {
   done := make(chan string)
   size,_ := strconv.Atoi(os.Args[1]) // like atoi() function in C
   ps,_ := strconv.Atoi(os.Args[2])
   cs,_ := strconv.Atoi(os.Args[3])

   bb := make(chan string, size)

   for i:=0; i < ps; i++ {
      go producer(bb,i)
   }
   for i:=0; i < cs; i++ {
      go consumer(bb,done,i)
   }
   <-done
}
