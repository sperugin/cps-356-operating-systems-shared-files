package main

import (
   "fmt"
   "time"
   "os"
   "sync"
   "strconv"
)

var wg = &sync.WaitGroup{}

func producer(bb chan<- string) {
    var timestr string

    for i:=0; i<10; i++ {
        timestr = time.Now().Local().Format("15:04:05.000")
        bb<-timestr
        fmt.Fprintf(os.Stderr, "Producer just put %s in the shared buffer.\n", timestr)
    }
    wg.Done()
}


func consumer(bb <-chan string) {
   for i:=0; i<10; i++ {
      item := <-bb
      time.Sleep(time.Second*2) // time to consume
      fmt.Fprintf(os.Stderr, "Consumer just consumed %s from the shared buffer.\n", item)
   }
   wg.Done()
}

func main() {
   size,_ := strconv.Atoi(os.Args[1]) // like atoi() function in C
   bb := make(chan string, size)
   go producer(bb)
   go consumer(bb)
   wg.Add(2)
   wg.Wait()
}
