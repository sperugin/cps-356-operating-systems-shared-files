package main

import (
   "os"
   "time"
   "fmt"
   "strconv"
)

// still has a race-condition, but that race condition is not deadlock
// fix the race-condition by removing the unnecessary select stmt in consumer

func producer(buf chan<- string) {
    var timestr string
    for i:= 0; i<10; i++ {
        timestr = time.Now().Local().Format("15:04:05.000")
        buf <- timestr
        fmt.Printf("Producer just put %s in the shared buffer.\n", timestr)
        if i == 4 {
            time.Sleep(time.Second*5) 
        }
    }
}

func consumer(buf <-chan string, goodbye chan<- int) {
      //time.Sleep(time.Second*2) // time to consume item
   for {
      select {
          case item := <- buf:
//             time.Sleep(time.Second*2) // time to consume item
             fmt.Printf("Consumer just consumed %s from the shared buffer.\n", item)
          default:
             goodbye <- 1
      }
   }
}

func main() {
   var finish = make(chan int)

   length,_ := strconv.Atoi(os.Args[1]) // like atoi() function in C

   buffer := make(chan string, length)

   go producer(buffer)
   
   time.Sleep(time.Second*2)
   go consumer(buffer,finish)
   <- finish
}
