package main

import (
   "fmt"
   "time"
   "os"
   "strconv"
   "sync"
)

var wg = &sync.WaitGroup{}

func producer(b chan<- string, i int) {
    var timestr string

    timestr = time.Now().Local().Format("15:04:05.000")
    b<-timestr
    fmt.Fprintf(os.Stderr, "Producer %d just put %s in the shared buffer.\n", i, timestr)

    wg.Done()
}


func consumer(t <-chan string, i int) {
   time.Sleep(time.Second*2) // time to consume
   item := <-t
   fmt.Fprintf(os.Stderr, "Consumer %d just consumed %s from the shared buffer.\n", i, item)
   wg.Done()
}

func main() {
   size,_ := strconv.Atoi(os.Args[1]) // like atoi() function in C
   ps,_ := strconv.Atoi(os.Args[2])
   cs,_ := strconv.Atoi(os.Args[3])

   bb := make(chan string, size)

   for i:=0; i < ps; i++ {
      go producer(bb,i)
   }
   for i:=0; i < cs; i++ {
      go consumer(bb,i)
   }
   wg.Add(ps+cs)
   wg.Wait()
}
