package main

import (
  "fmt"
)

var end chan int

func work (c chan int) {
   x := <-c
   fmt.Print(x)
   end <- 1
}

func main() {
   end = make(chan int)
   comm := make(chan int,1)

   comm <- 1

   go work(comm)

   <-end
}
