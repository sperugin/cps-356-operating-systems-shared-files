package main

import (
    "sync"
    "time"
    "math/rand"
    "fmt"
    "os"
    "strconv"
)

var wg = &sync.WaitGroup{}
var capacity int
var num_guests int
var limit int

var wakeup chan string
var keg chan int
var key chan string

func Pledge() {
  for {
     fmt.Printf("Pledge is sleeping in toolshed.\n")
     <- wakeup
     fmt.Printf("Pledge just wokeup.\n")
     refillKeg()
     fmt.Printf("Pledge just refilled keg.\n")
     time.Sleep(time.Second*2) 
     key <- "available"
  }
}

func refillKeg() {
   for i:=capacity; i>0; i-- {
     keg <- i
   }
}

func Guest(j int) {
   for i:=0; i<limit; i++ {
      getServing(j,true)
      time.Sleep(time.Second*2)
   }
   wg.Done()
   fmt.Printf("Guest %d is tired and leaving party.\n", j)
}

func drink(i int, s int) {
   fmt.Printf("Guest %d is drinking.\n", i)
   time.Sleep(time.Duration(rand.Int63n(1e9)))
   fmt.Printf("Guest %d is done drinking. [keg has %d servings remaining]\n", i, s-1)
}

func wakeupPledge(i int) {
   fmt.Printf("Guest %d just wokeup pledge.\n", i)
   wakeup <- "wakeup, and refill keg, please"
}

func getServing(i int, firsttime bool) {
  if firsttime {
     fmt.Printf("Guest %d is attempting to retrieve a serving.\n", i)
  }
  select {
     case s:= <- keg:
        drink(i,s)
     default:
        select {
           case <- key:
              wakeupPledge(i)          
              getServing(i,false)
           default:
              getServing(i,false)
        }
  }
}

func main() {

   capacity,_ = strconv.Atoi(os.Args[1])
   num_guests,_ = strconv.Atoi(os.Args[2])
   limit,_ = strconv.Atoi(os.Args[3])

   wakeup = make(chan string)
   keg = make(chan int, capacity)
   key = make(chan string, 1)

   refillKeg()

   key <- "available"

   go Pledge()

   for i:=1; i<=num_guests; i++ {
      go Guest(i)
   }

   wg.Add(num_guests)
   wg.Wait()
}
