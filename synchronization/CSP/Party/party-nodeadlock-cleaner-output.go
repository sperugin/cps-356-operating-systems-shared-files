package main

import (
    "sync"
    "time"
    "math/rand"
    "fmt"
    "os"
    "strconv"
)

type serving struct {
   refill_num int
   serving_num int
}

var wg = &sync.WaitGroup{}
var capacity int
var num_guests int
var limit int

var wakeup chan string
var keg chan serving
var key chan string

func Pledge(refill_num int) {
  for {
     fmt.Printf("Pledge is sleeping in toolshed.\n")
     <- wakeup
     fmt.Printf("Pledge just wokeup.\n")
     refillKeg(refill_num)
     fmt.Printf("Pledge just refilled keg.\n")
     time.Sleep(time.Second*2) 
     key <- "available"
     refill_num++
  }
}

func refillKeg(n int) {
   for i:=capacity; i>0; i-- {
     keg <- serving{n,i}
   }
}

func Guest(j int) {
   for i:=0; i<limit; i++ {
      getServing(j,true)
      time.Sleep(time.Second*2)
   }
   wg.Done()
   fmt.Printf("Guest %d is tired and leaving party.\n", j)
}

func drink(i int, s serving) {
   fmt.Printf("Guest %d is drinking.\n", i)
   time.Sleep(time.Duration(rand.Int63n(1e9)))
   fmt.Printf("Guest %d is done drinking. [refill %d has %d servings remaining]\n",
              i, s.refill_num, s.serving_num-1)
}

func wakeupPledge(i int) {
   fmt.Printf("Guest %d just wokeup pledge.\n", i)
   wakeup <- "wakeup, and refill keg, please"
}

func getServing(i int, firsttime bool) {
  if firsttime {
     fmt.Printf("Guest %d is attempting to retrieve a serving.\n", i)
  }
  select {
     case s:= <- keg:
        drink(i,s)
     default:
        select {
           case <- key:
              wakeupPledge(i)          
              getServing(i,false)
           default:
              getServing(i,false)
        }
  }
}

func main() {

   capacity,_ = strconv.Atoi(os.Args[1])
   num_guests,_ = strconv.Atoi(os.Args[2])
   limit,_ = strconv.Atoi(os.Args[3])

   wakeup = make(chan string)
   keg = make(chan serving, capacity)
   key = make(chan string, 1)

   refillKeg(0)

   key <- "available"

   go Pledge(1)

   for i:=1; i<=num_guests; i++ {
      go Guest(i)
   }

   wg.Add(num_guests)
   wg.Wait()
}
