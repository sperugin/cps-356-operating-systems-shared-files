package main

import (
    "sync"
    "time"
    "math/rand"
    "fmt"
    "os"
    "strconv"
)

var wg = &sync.WaitGroup{}
var capacity int
var num_guests int
var limit int

var wakeup chan string
var keg chan int
var key chan string
var servings chan int

func Pledge() {
  for {
     fmt.Printf("Pledge is sleeping in toolshed.\n")
     <- wakeup
     fmt.Printf("Pledge just wokeup.\n")
     refillKeg()
     fmt.Printf("Pledge just refilled keg.\n")
     time.Sleep(time.Second*2)
     key <- "available"
     servings <- capacity
  }
}

func refillKeg() {
   for i:=1; i<=capacity; i++ {
     keg <- 1
   }
}

func Guest(j int) {
   for i:=0; i<limit; i++ {
      getServing(j)
      time.Sleep(time.Second*2)
   }
   wg.Done()
   fmt.Printf("Guest %d is tired and leaving party.\n", j)
}

func drink(i int) {
   <- keg
   fmt.Printf("Guest %d is drinking.\n", i)
   howmany := <- servings
   servings <- (howmany-1)
   time.Sleep(time.Duration(rand.Int63n(1e9)))
   fmt.Printf("Guest %d is done drinking. [servings remaining = %d]\n",
              i, howmany-1)
}

func wakeupPledge(i int) {
   fmt.Printf("Guest %d just wokeup pledge.\n", i)
   wakeup <- "wakeup, and refill keg, please"
}

func getServing(i int) {
  fmt.Printf("Guest %d is attempting to retrieve a serving.\n", i)
  s := <- servings
  if s == 0 {
     select {
        case <- key:
           wakeupPledge(i)          
           getServing(i)
        default:
           drink(i)
     }
  } else {
      servings <- s
      drink(i)
    }
}

func main() {

   capacity,_ = strconv.Atoi(os.Args[1])
   num_guests,_ = strconv.Atoi(os.Args[2])
   limit,_ = strconv.Atoi(os.Args[3])

   wakeup = make(chan string)
   keg = make(chan int, capacity)
   servings = make(chan int, 1)
   key = make(chan string, 1)

   refillKeg()

   key <- "available"
   servings <- capacity

   go Pledge()

   for i:=1; i<=num_guests; i++ {
      go Guest(i)
   }

   wg.Add(num_guests)
   wg.Wait()
}
