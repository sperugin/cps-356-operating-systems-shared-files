package main

import (
   "fmt"
   //"time"
)

var done = make(chan string)

func work(c chan int) {
   x := <-c
   fmt.Printf("%d\n", x)
   done <- "I'm done"
}

func main() {
   comm := make(chan int, 2)
   go work(comm)
   comm <- 1
   <- done
}
