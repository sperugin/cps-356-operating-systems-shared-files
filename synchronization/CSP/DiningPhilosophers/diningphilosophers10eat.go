package main

import (
    "time"
    "fmt"
    "os"
    "strconv"
    "math/rand"
)

type Philosopher struct {
    name string
    mychopstick chan string
    neighbor *Philosopher
}

func makePhilosopher(name string, neighbor *Philosopher) *Philosopher {
    phil := &Philosopher{name, make(chan string, 1), neighbor}
    phil.mychopstick <- "whatever"
    return phil
}

func (phil *Philosopher) dine(fuller chan *Philosopher, num_eat int) {
   for i:=0; i<num_eat; i++ {
      phil.think();
      <- phil.mychopstick 
      fmt.Printf("%s has acquired their chopstick.\n", phil.name);
      <- phil.neighbor.mychopstick 
      fmt.Printf("%s has acquired their neighbor's chopstick.\n", phil.name);
      phil.eat();
      phil.mychopstick <- "available"
      fmt.Printf("%s has released their chopstick.\n", phil.name);
      phil.neighbor.mychopstick <- "available"
      fmt.Printf("%s has released their neighbor's chopstick.\n", phil.name);
   }
   fuller <- phil
}

func (phil *Philosopher) think() {
   time.Sleep(time.Duration(rand.Int63n(1e9)))
   fmt.Printf("%s is thinking.\n", phil.name);
}

func (phil *Philosopher) eat() {
   time.Sleep(time.Duration(rand.Int63n(1e9)))
   fmt.Printf("%s is eating.\n", phil.name);
}

func main() {

   num_eat,_ := strconv.Atoi(os.Args[1])

   names := []string{"Plato", "Aristotle", "Augustine", "Acquinas", "Stein"}

   philosophers := make([]*Philosopher, len(names))

   var phil *Philosopher 
   full := make(chan *Philosopher)

   for i, name := range names {
      phil = makePhilosopher(name, phil)   
      philosophers[i] = phil
   }
   philosophers[0].neighbor = phil

  fmt.Printf("There are %d philosophers sitting at the table.\n", len(names))

  for _,phil := range philosophers {
     go phil.dine(full,num_eat)
  }

  for i:=0; i<len(philosophers); i++ {
      phil := <- full
      fmt.Printf("%s has finished dining.\n", phil.name)
  }
}
