package main

import (
   "os"
   "fmt"
   "math/rand"
   "time"
   "strconv"
)

func random(min, max int) int {
   rand.Seed(time.Now().Unix())
   return rand.Intn(max - min) + min
}

type Philosopher struct {
   name      string
   mychopstick chan string
   //mychopstick chan bool
   neighbor  *Philosopher
   num_eat  int
}

func makePhilosopher(name string, neighbor *Philosopher, num_eat int) *Philosopher {
   phil := &Philosopher{name, make(chan string, 1), neighbor, num_eat}
   phil.mychopstick <- "available"
   //phil.mychopstick <- true
   return phil
}

func (phil *Philosopher) think() {
   fmt.Printf("%s is thinking.\n", phil.name)
   time.Sleep(time.Duration(rand.Int63n(1e9)))
}

func (phil *Philosopher) eat() {
   fmt.Printf("%s is eating.\n", phil.name)
   time.Sleep(time.Duration(rand.Int63n(1e9)))
}

func (phil *Philosopher) requestChopsticks() {
   timeout := make(chan string, 1)
   go func() { time.Sleep(1e9); timeout <- "timesup!" }()
   <-phil.mychopstick
   time.Sleep(time.Second*2)


   fmt.Printf("%s has now acquired one chopstick.\n", phil.name)
   select {
      case <-phil.neighbor.mychopstick:
         fmt.Printf("%s has now acquired %s's chopstick.\n", phil.name, phil.neighbor.name)
         fmt.Printf("%s has two chopsticks now.\n", phil.name)
         return
      case <-timeout:
         phil.mychopstick <- "available"
         //phil.mychopstick <- true
         phil.think()
         phil.requestChopsticks()
   }
}

func (phil *Philosopher) releaseChopsticks() {
   phil.mychopstick <- "available"
   //phil.mychopstick <- false
   phil.neighbor.mychopstick <- "available"
   //phil.neighbor.mychopstick <- false
}

func (phil *Philosopher) dine(full chan *Philosopher, num_eat int) {
   for i:=0; i<num_eat; i++ {
      phil.think();
      <- phil.mychopstick
      fmt.Printf("%s has acquired their chopstick.\n", phil.name);
      time.Sleep(time.Second*2)
      <- phil.neighbor.mychopstick
      fmt.Printf("%s has acquired their neighbor's chopstick.\n", phil.name);
      phil.eat();
      phil.mychopstick <- "available"
      fmt.Printf("%s has released their chopstick.\n", phil.name);
      phil.neighbor.mychopstick <- "available"
      fmt.Printf("%s has released their neighbor's chopstick.\n", phil.name);
   }
   full <- phil
}

func main() {

   max_eat,_ := strconv.Atoi(os.Args[1])
   names := []string{"Plato", "Aristotle", "Augustine", "Aquinas", "Stein"}

   philosophers := make([]*Philosopher, len(names))

   var phil *Philosopher

   for i, name := range names {
      phil = makePhilosopher(name, phil, random(1, max_eat))
      philosophers[i] = phil
   }
   philosophers[0].neighbor = phil
   fmt.Printf("There are %d philosophers sitting at a dining table.\n", len(philosophers))
   // they each have one chopstick, and must borrow from their neighbor to eat.

   full := make(chan *Philosopher)
   for _, phil := range philosophers {
      go phil.dine(full,phil.num_eat)
   }

   for i := 0; i < len(names); i++ {
      phil := <-full
      fmt.Printf("%s is done dining.\n", phil.name)
   }
}
