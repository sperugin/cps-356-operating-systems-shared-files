import java.util.concurrent.Semaphore;

class Solution {

   Semaphore p;

   Solution(Semaphore p) {
     this.p = p;
   }

   public void entry(...) {
      ... p.acquire();
   }

   public void exit (...) {
     .... p.release();
   }
}
