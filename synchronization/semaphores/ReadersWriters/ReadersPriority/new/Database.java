/**
 * Database.java
 *
 * This class contains the methods the readers and writers will use
 * to coordinate access to the database.
 *
 */
import java.util.concurrent.Semaphore;

class Database implements RWLock {
   // the number of active readers
   Semaphore readerCount;

   // provides mutex for writers
   // also used by readers to prevent writers to enter a db being read
   Semaphore db;

   public Database() {
      readerCount = new Semaphore(0, true);
      db = new Semaphore(1, true);
   }

   // reader will call this when they start reading
   public void acquireReadLock(int readerNum) {
      readerCount.release();


      // if I am the first reader tell all other
      // that the database is being read
      if (readerCount.availablePermits() == 1) {
         try { db.acquire(); } catch (InterruptedException e) { }
         System.err.println("Reader " + readerNum + " has acquired db semaphore. Reader count = " + readerCount.availablePermits());
      }

      System.err.println("Reader " + readerNum + " is reading. Reader count = " + readerCount.availablePermits());
   }

   public void releaseReadLock(int readerNum) {
      try { readerCount.acquire(); } catch (InterruptedException e) { }

      // if I am the last reader tell all others
      // that the database is no longer being read
      if (readerCount.availablePermits() == 0) {
         db.release();
         System.err.println("Reader " + readerNum + " signaled db semaphore. Reader count = " + readerCount.availablePermits());
 
      }

      System.err.println("Reader " + readerNum + " is done reading. Reader count = " + readerCount.availablePermits());
   }

   // writer will call this when they start writing
   public void acquireWriteLock(int writerNum) {

      try { db.acquire(); } catch (InterruptedException e) { }

      System.err.println("Writer " + writerNum + " is writing.");
   }

   // writer will call this when they stop writing
   public void releaseWriteLock(int writerNum) {
      db.release();

      System.err.println("Writer " + writerNum + " is done writing.");
   }
}
