/**
 * Database.java
 *
 * This class contains the methods the readers and writers will use
 * to coordinate access to the database.
 *
 */
import java.util.concurrent.Semaphore;

class Database implements RWLock {
   // the number of active readers
   int readerCount;

   // provides mutex for readerCount
   Semaphore mutex;

   Semaphore writers_waiting;
   Semaphore readers_waiting;

   // provides mutex for writers
   // also used by readers to prevent writers to enter a db being read
   Semaphore db;

   public Database() {
     
      readerCount = 0;

      mutex = new Semaphore(1, true);
      db = new Semaphore(1, true);

      writers_waiting = new Semaphore(0, true);
      readers_waiting = new Semaphore(0, true);
   }

   // reader will call this when they start reading
   public void acquireReadLock(int readerNum) {
      if (writers_waiting.hasQueuedThreads()) 
         try { readers_waiting.acquire(); } catch (InterruptedException e) { }

      try { mutex.acquire(); } catch (InterruptedException e) { }

      // if I am the first reader tell all other
      // that the database is being read
      if (readerCount == 0) {
         try { db.acquire(); } catch (InterruptedException e) { }
         System.err.println("Reader " + readerNum + " has acquired db semaphore. Reader count = " + readerCount);
      }
      ++readerCount;

      System.err.println("Reader " + readerNum + " is reading. Reader count = " + readerCount);

      mutex.release();
   }

   public void releaseReadLock(int readerNum) {
      try { mutex.acquire(); } catch (InterruptedException e) { }

      // if I am the last reader tell all others
      // that the database is no longer being read
      if (readerCount == 1) {
         db.release();
         System.err.println("Reader " + readerNum + " signaled db semaphore. Reader count = " + readerCount);
         if (writers_waiting.hasQueuedThreads()) {
            writers_waiting.release();
            System.err.println("Reader " + readerNum + " signaled writer waiting semaphore. Reader count = " + readerCount);
         }
      }
      --readerCount;

      System.err.println("Reader " + readerNum + " is done reading. Reader count = " + readerCount);
      mutex.release();
   }

   // writer will call this when they start writing
   public void acquireWriteLock(int writerNum) {

      if (writers_waiting.hasQueuedThreads()) {
         try { writers_waiting.acquire(); } catch (InterruptedException e) { }
         try { db.acquire(); } catch (InterruptedException e) { }
      } else
          try { if (db.tryAcquire())
                   System.err.println("Writer " + writerNum + " is writing.");
                else {
                   writers_waiting.acquire();
                   db.acquire();
                }
              } catch (InterruptedException e) { }
                }

// This releaseWriteLock method makes this solution writers' priority (without
// addressing starvation) b/c the writers can starve out the readers, but the
// readers cannot starve out the writers.

   // writer will call this when they stop writing
/*
   public void releaseWriteLock(int writerNum) {
      db.release();

      System.err.println("Writer " + writerNum + " is done writing.");

      if (writers_waiting.hasQueuedThreads())
         writers_waiting.release();
      else if (readers_waiting.hasQueuedThreads()) {
         System.err.println("Writer " + writerNum + " is about to release " + readers_waiting.getQueueLength() + " readers.");
         // signal all the readers waiting on the writing writer
         readers_waiting.release(readers_waiting.getQueueLength());
      }

   }
*/

// This releaseWriteLock method makes this solution readers' priority without
// starvation b/c the readers can no longer starve out the writers,
// but the solution is not
// fair because the order in which readers and writers arrive is not
// necessarily the order in which they access the database.

   // writer will call this when they stop writing
   public void releaseWriteLock(int writerNum) {
      db.release();

      System.err.println("Writer " + writerNum + " is done writing.");

      if (readers_waiting.hasQueuedThreads()) {
         System.err.println("Writer " + writerNum + " is about to release " + readers_waiting.getQueueLength() + " readers.");
         // signal all the readers waiting on the writing writer
         readers_waiting.release(readers_waiting.getQueueLength());
      } else if (writers_waiting.hasQueuedThreads())
                writers_waiting.release();
   }
}
