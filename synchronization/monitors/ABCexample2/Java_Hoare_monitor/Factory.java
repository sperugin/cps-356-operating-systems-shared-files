import java.util.Random;

class Factory {

   public static void main(String args[]) {
      Random generator = new Random();
      char type = 'A';
      int r;
      Monitor monitor = new Monitor();

      for (int i = 0; i < 100; i++) {
      //for (int i = 0; i < 10; i++) {
         // generate random number between 1 and 3
         r = Math.abs (generator.nextInt()) % 3 + 1;

         if (r == 1)
            type = 'A';
         else if (r == 2)
            type = 'B';
         else if (r == 3)
            type = 'C';
  
         (new Thread(new Thread1(type, i, monitor))).start();
      }
   }
}
