import java.util.concurrent.*;
import java.util.concurrent.locks.*;

// ME among Cs
// maximum 5 Bs in at one time
// ME between As and Bs
// ME between As and Cs
// priority to alternate thread type

class Monitor {

   ReentrantLock lock;
   Condition stopA, stopB, stopC;

   boolean CisRunning;
   int numArunning, numBrunning;

   Monitor() {
      lock = new ReentrantLock();
      stopA = lock.newCondition();
      stopB = lock.newCondition();
      stopC = lock.newCondition();
  
      CisRunning = false;
      numArunning = 0;
      numBrunning = 0;
   }

   public void Aentry() {
      lock.lock();
      if (numBrunning > 0 || CisRunning)
         try { stopA.await(); } catch (InterruptedException ie) { }
      numArunning++;
      lock.unlock();
   }

   public void Aexit() {
      lock.lock();
      numArunning--;
      if (numArunning == 0) {
         stopC.signal();
         // signal up to 5 B's
         stopB.signal();
         stopB.signal();
         stopB.signal();
         stopB.signal();
         stopB.signal();
      }
      lock.unlock();
   }

   public void Bentry() {
      lock.lock();
      if (numArunning > 0 || numBrunning == 5)
         try { stopB.await(); } catch (InterruptedException ie) { }
      numBrunning++;
      lock.unlock();
   }

   public void Bexit() {
      lock.lock();
      numBrunning--;

      if (numBrunning == 0 && !CisRunning && lock.hasWaiters(stopA))
         stopA.signalAll();
      else stopB.signal();

      lock.unlock();
   }

   public void Centry() {
      lock.lock();
      if (CisRunning || numArunning > 0)
         try { stopC.await(); } catch (InterruptedException ie) { }
      CisRunning = true;
      lock.unlock();
   }

   public void Cexit() {
      lock.lock();
      CisRunning = false;
      if (numBrunning == 0 && lock.hasWaiters(stopA))
         stopA.signalAll();
      else stopC.signal();
      lock.unlock();
   }
}
