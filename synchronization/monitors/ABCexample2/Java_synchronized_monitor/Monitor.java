// ME among Cs
// maximum 5 Bs in at one time
// ME between As and Bs
// ME between As and Cs
// priority to alternate thread type

class Monitor {

   boolean CisRunning;
   int numArunning, numBrunning;

   Monitor() {
      CisRunning = false;
      numArunning = 0;
      numBrunning = 0;
   }

   public synchronized void Aentry() {
      while (numBrunning > 0 || CisRunning)
         try { wait(); } catch (InterruptedException ie) { }
      numArunning++;
   }

   public synchronized void Aexit() {
      numArunning--;
      notifyAll();
   }

   public synchronized void Bentry() {
      while (numArunning > 0 || numBrunning == 5)
         try { wait(); } catch (InterruptedException ie) { }
      numBrunning++;
   }

   public synchronized void Bexit() {
      numBrunning--;
      notifyAll();
   }

   public synchronized void Centry() {
      while (CisRunning || numArunning > 0)
         try { wait(); } catch (InterruptedException ie) { }
      CisRunning = true;
   }

   public synchronized void Cexit() {
      CisRunning = false;
      notifyAll();
   }
}
