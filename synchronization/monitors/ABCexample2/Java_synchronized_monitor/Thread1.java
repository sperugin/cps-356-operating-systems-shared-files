class Thread1 implements Runnable {

   Monitor monitor;
   char type;
   int id;

   Thread1 (char type, int id, Monitor monitor) {
      this.type = type;
      this.id = id;
      this.monitor = monitor;
   }

   public void run() {

      if (type == 'A')
         monitor.Aentry();
      else if (type == 'B')
         monitor.Bentry();
      else if (type == 'C')
         monitor.Centry();

      System.err.println (type + " " + id + " just started.");
      SleepUtilities.nap();
      System.err.println (type + " " + id + " is running.");
      SleepUtilities.nap();
      System.err.println (type + " " + id + " is finished.");

      if (type == 'A')
         monitor.Aexit();
      else if (type == 'B')
         monitor.Bexit();
      else if (type == 'C')
         monitor.Cexit();
   }
}
