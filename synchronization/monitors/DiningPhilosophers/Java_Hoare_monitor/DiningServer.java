interface DiningServer {
   // called by a philosopher when the philosopher get hungry
   public void pickupForks(int id);

   // called by a philosopher when the philosopher is finished eating
   public void releaseForks(int id);
}
