class Table {
   static final int NUM_PHILOSOPHERS = 5;

   public static void main(String args[]) {
      DiningPhilosophersMonitor dpm =
      new DiningPhilosophersMonitor(NUM_PHILOSOPHERS);

      for (int i = 0; i < NUM_PHILOSOPHERS; i++) {
         System.err.println (i + " just started");
         (new Thread(new Philosopher(i, dpm))).start();
      }
   }
}
