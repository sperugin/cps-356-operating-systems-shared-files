import java.lang.Math;

class Philosopher implements Runnable {

   DiningPhilosophersMonitor dpm;
   int id;

   Philosopher (int id, DiningPhilosophersMonitor dpm) {
      this.id = id;
      this.dpm = dpm;
   }

   public void run() {
      System.err.println ("Philosopher " + id + " just started.");

      while (true) {
         dpm.pickupForks(id);
         eat();
         dpm.releaseForks(id);
      }
   }

   private void eat() {
      System.err.println ("Philosopher " + id + " is eating.");
      try { Thread.sleep((int) Math.random()*1000); } catch (Exception e) { }
   }
}
