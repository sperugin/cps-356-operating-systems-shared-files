import java.util.concurrent.*;
import java.util.concurrent.locks.*;

class Monitor {

   Monitor(...) {

   }

   public synchronized void entry(int id) {
      ... wait();
   }

   public synchronized void exit (int id) {
     .... notify() or notifyAll()
   }
}
