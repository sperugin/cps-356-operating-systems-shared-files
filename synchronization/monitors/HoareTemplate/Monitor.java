import java.util.concurrent.*;
import java.util.concurrent.locks.*;

class Monitor {

   Lock lock;
   Condition stopA, ...

   Monitor(...) {
      lock = new ReentrantLock();

      stopA = new Condition();

      stopA = lock.newCondition();

   }

   public void entry(int id) {
      lock.lock();
      ... 
      lock.unlock();
   }

   public void exit (int id) {
      lock.lock();
      ...
      lock.unlock();
   }
}
