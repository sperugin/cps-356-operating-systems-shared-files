import java.lang.Math;

class Thread1 implements Runnable {

   Monitor m;
   int id;

   Thread1 (int id, Monitor m) {
      this.id = id;
      this.m = m;
   }

   public void run() {
      System.err.println ("Thread1 " + id + " just started.");

      while (true) {
         m.entry(id);
         cs();
         m.exit(id);
      }
   }

   private void cs() {
      System.err.println ("Thread1 " + id + " is eating.");
      try { Thread.sleep((int) Math.random()*1000); } catch (Exception e) { }
   }
}
