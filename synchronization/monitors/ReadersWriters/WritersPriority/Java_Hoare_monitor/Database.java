/**
 * Database.java
 *
 * This class contains the methods the readers and writers will use
 * to coordinate access to the database.
 *
 */

import java.util.concurrent.*;
import java.util.concurrent.locks.*;

class Database implements RWLock {

   ReentrantLock lock;
   Condition stopW;
   Condition stopR;

   // the number of active readers
   int readerCount;
 
   // is the db currently being written
   boolean dbWriting;

   Database() {
      lock = new ReentrantLock();

      stopR = lock.newCondition();
      stopW = lock.newCondition();
      readerCount = 0;
      dbWriting = false;
   }

   // reader will call this when they start reading
   public void acquireReadLock(int readerNum) {
      lock.lock();

      if (dbWriting == true || lock.hasWaiters(stopW))
         try { stopR.await(); } catch (InterruptedException e) { }

      ++readerCount;

      System.err.println("reader " + readerNum + " is reading. reader count = " + readerCount);
      lock.unlock();
   }

   public void releaseReadLock(int readerNum) {
      lock.lock();
      --readerCount;

      System.err.println("reader " + readerNum + " is done reading. reader count = " + readerCount);

      // if I am the last reader tell _a_ waiting writer
      // that the database is no longer being read
      if (readerCount == 0)
         stopW.signal();
      lock.unlock();
   }

   // writer will call this when they start writing
   public void acquireWriteLock(int writerNum) {
      lock.lock();

      if (readerCount > 0 || dbWriting == true)
         try { stopW.await(); } catch (InterruptedException e) { }
 
      // once there are either no readers or writers
      // indicate that the database is being written
      dbWriting = true;

      System.err.println("writer " + writerNum + " is writing.");
      lock.unlock();
   }

   // writer will call this when they stop writing
   public void releaseWriteLock(int writerNum) {
      lock.lock();
      dbWriting = false;

      System.err.println("writer " + writerNum + " is done writing.");

      if (lock.hasWaiters(stopR))
         stopR.signalAll();
      else
         stopW.signal();

      lock.unlock();
   }
}
