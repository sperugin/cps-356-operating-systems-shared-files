A monitor solution to the (readers' priority) Readers-Writers problem
(without addressing starvation):

READER                      WRITER
------------------------------------
Rentry();                  Wentry();
read();                    write();
Rexit();                   Wexit();

Conditon stopW, stopR;   
int readerCount = 0;
int reader_waiting = 0;
bool writer_writing = false;
int writers_waiting = 0;

Rentry
------
if (writer_writing) {
   reader_waiting++;
   stopR.wait();
   reader_waiting--;
}
readerCount++;

Rexit
-----
readerCount--;
if (readerCount == 0)
   stopW.signal();

Wentry
------
if (readerCount > 0 || wwriting) {
   writer_waiting++;
   stopW.wait();
   writers_waiting--;
}
writer_writing = true;

// This Wexit makes this solution readers' priority b/c the readers can starve
// out the writers, but the writers cannot starve out the readers
Wexit
-----
writer_writing = false;
if (reader_waiting > 0)
   while (reader_waiting > 0)
      stopR.signal();
else stopW.signal();

// This Wexit makes this solution neither readers' nor writers' priority b/c the
// writers can starve out the readers just as easy as the readers can starve out
// the writers.
Wexit
-----
writer_writing = false;
if (writer_waiting > 0)
   stopW.signal();
else
   while (reader_waiting > 0)
      stopR.signal();

The entry and exit code is in the monitor.  The monitor protects this code.
This is the entire purpose of the monitor.
