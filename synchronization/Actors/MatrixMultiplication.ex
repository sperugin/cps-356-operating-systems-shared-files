defmodule MatrixMultiplication do
  def multiply(a_lists, b_lists) do
    {a, row_count, vector_length} = matrix_to_map(a_lists, 0, 0)
    {b, ^vector_length, col_count} = matrix_to_map(b_lists, 0, 0)
    start_workers(a, b, row_count, vector_length, col_count)
    result_map = get_result(%{}, row_count * col_count)

    IO.write ("\nProduct Matrix (A x B) as a map:\n")
    IO.write("#{inspect result_map}\n\n")

    IO.write ("Product Matrix (A x B) as a list of lists: \n")
    result_matrix = map_to_matrix(result_map, row_count, col_count)

    #IO.write("#{inspect result_matrix}\n\n")


    result_matrix
  end
  
  def matrix_to_map([], i, _), do: {%{}, i, 0}
  def matrix_to_map([[]|more_rows], i, j) do
    {partial_map, row_count, _} = matrix_to_map(more_rows, i+1, 0)
    {partial_map, row_count, j}
  end
  def matrix_to_map([[item|row_tail]|more_rows], i, j) do
    {partial_map, row_count, col_count} = matrix_to_map([row_tail|more_rows], i, j+1)
    {Map.put(partial_map, {i, j}, item), row_count, col_count}
  end
  
  def map_to_matrix(map, row_count, col_count) do
    # Use 0-index ranges here to simplify the recursion
    # For each row number, generate the row by looking up each column for each column number
    Enum.map(
      0..(row_count-1),
      fn(row) -> Enum.map(0..(col_count-1), fn(col) -> map[{row, col}] end) end
    )
  end
  
  def start_workers(a, b, row_count, vector_length, col_count) do
    # Use 0-index ranges here to simplify the recursion
    # For each row/column number pair, start a worker process
    Enum.map(
      0..(row_count-1),
      fn(row) -> Enum.map(0..(col_count-1), fn(col) ->
         spawn_link(__MODULE__, :work, [self(), a, b, row, col, vector_length]) end) end
    )
  end
  
  def get_result(result, 0), do: result
  def get_result(partial_result, remaining) do
    receive do
      ######################################################################
      ...
      ######################################################################
    end
  end
  
  def work(sender, a, b, i, j, vector_length) do
    ###########################################################################
    ...
    ###########################################################################
  end
  
  def multiply_vector(_, _, _, _, -1) do
    0
  end

  def multiply_vector(a, b, i, j, k) do
    IO.puts "multiplying: (#{i}, #{k}) * (#{k}, #{j}) = #{a[{i, k}]} * #{b[{k, j}]} = #{a[{i, k}] * b[{k, j}]}"
    (a[{i, k}] * b[{k, j}]) + multiply_vector(a, b, i, j, k-1)
  end
end

#test data
#a = [
#  [1, 2],
#  [3, 4]
#]
#b = [
#  [2, 3],
#  [4, 5]
#]

a = [
  [1, 3, 5, 7],
  [2, 4, 6, 8]
]
b = [
  [1, 3, 5],
  [2, 4, 6],
  [1, 2, 3],
  [7, 5, 3]
]
#The main thread is the master thread. In other
#examples, the main thread essentially just starts the other actors and decides
#when to quit. But this program requires the actor that calls the multiply
#function to wait for a response. We could have started a separate "master"
#thread, but that was unnecessary as "real life" usage would just have the
#calling actor act as the "master".

IO.write ("Matrix A as a list of lists:\n")
IO.write("#{inspect a}\n\n")
IO.write ("Matrix A as a map:\n")
{map_a,rowsa,colsa} = MatrixMultiplication.matrix_to_map(a,0,0)
IO.write("#{inspect map_a}\n\n")

IO.write ("Matrix B as a list of lists:\n")
IO.write("#{inspect b}\n\n")
IO.write ("Matrix B as a map:\n")
{map_b,rowsb,colsb} = MatrixMultiplication.matrix_to_map(b,0,0)
IO.write("#{inspect map_b}\n\n")

IO.write ("Parallel Matrix Multiplication Operations on Maps A and B:\n\n")
IO.write("#{inspect MatrixMultiplication.multiply(a,b),char_lists: false}\n\n")
