defmodule Upper do

   def all_upper?(s) do
      cond do
         s == nil -> false
         true -> String.upcase(s) == s
      end
   end
end
