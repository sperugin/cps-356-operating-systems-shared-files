# main does not wait

# uses registered Actor names

# count passed on each recursive call; count not passed in each message

defmodule PingPong do

   def play() do
      send(:ping_actor, {:ping})
   end

   def new_ping(c) do
      pid = spawn_link(__MODULE__, :listenPing, [c])
      Process.register(pid, :ping_actor)
   end

   def new_pong(c) do
      pid = spawn_link(__MODULE__, :listenPong, [c])
      Process.register(pid, :pong_actor)
   end

   def listenPing(count) do
    if count != 0 do
       receive do
          {:ping} ->
              IO.puts("ping")
              send(:pong_actor, {:pong})
       end
       listenPing(count-1)
    end
   end

   def listenPong(count) do
     if count !=0 do
       receive do
          {:pong} ->
              IO.puts("pong")
              send(:ping_actor, {:ping})
       end
       listenPong(count-1)
     end
   end
end

i = String.to_integer(hd(System.argv()))

PingPong.new_ping(i)
PingPong.new_pong(i)

PingPong.play()
