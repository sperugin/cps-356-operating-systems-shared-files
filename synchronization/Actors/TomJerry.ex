import :timer, only: [ sleep: 1 ]

defmodule TomJerry do

   def new_Tom() do
     pid = spawn_link(__MODULE__, :listen_tom, [])
     Process.register(pid, :tom_actor)
     pid
   end

   def new_Jerry() do
     pid = spawn_link(__MODULE__, :listen_jerry, [])
     Process.register(pid, :jerry_actor)
     pid
   end
 
   def listen_tom() do
      receive do
         {:go, :jerry_actor} ->
            IO.puts("This :go message came from Jerry.")
            IO.puts("Cool, but I'll reply to Jerry.")
            send(:jerry_actor, {:b, 38, 67}) 
            listen_tom1()

         {:go, pid} ->
            IO.puts("This :go message came from a non-Jerry actor.")
            send(:jerry_actor, {:b, 38, 67}) 
            listen_tom1()

         {:a, {greeting, task}} -> 
            cond do
              greeting > 10 ->
                 IO.puts("Tom just rec'd message :a with #{greeting} and #{task}")
                 send(:jerry_actor, {:b, 34, 67}) 
                 send(:jerry_actor, {:b, 4, 67}) 
                 listen_tom1()
              true ->
                 IO.puts("Tom is not exiting")
                 listen_tom1()
            end
      end
   end

   def listen_tom1() do
      receive do
         {:go, :jerry_actor} ->
            IO.puts("This :go message came from Jerry.")
            IO.puts("Cool, but I'll reply to Jerry.")
            listen_tom1()

         {:go, pid} ->
            IO.puts("This :go message came from a non-Jerry actor.")
            listen_tom1()

         {:a, {greeting, task}} -> 
            cond do
              greeting > 10 ->
                 IO.puts("Tom just rec'd message :a with #{greeting} and #{task}")
                 listen_tom1()
              true ->
                 IO.puts("Tom is not exiting")
                 listen_tom1()
            end
      end
   end

   def listen_jerry() do
      receive do
         {:b, 38, task1} ->
             IO.puts("This is the very first message from Tom.")
             send(:tom_actor, {:a, {35, 68}}) 
             send(:tom_actor, {:go, :jerry_actor}) 
             listen_jerry1()
         {:b, greeting, task} -> 
           cond do
               greeting > 10 ->
                 IO.puts("Jerry just rec'd message :b with #{greeting} and #{task}")
                 send(:tom_actor, {:a, {35, 68}}) 
                 send(:tom_actor, {:go, :jerry_actor}) 
                 send(:tom_actor, {:a, {5, 68}}) 
                 listen_jerry1()
               true ->
                 IO.puts("Tom is not exiting")
                 listen_jerry1()
           end
      end
   end
   def listen_jerry1() do
      receive do
         {:b, 38, task1} ->
             IO.puts("This is the very first message from Tom.")
             listen_jerry1()
         {:b, greeting, task} -> 
           cond do
               greeting > 10 ->
                 IO.puts("Jerry just rec'd message :b with #{greeting} and #{task}")
                 listen_jerry1()
               true ->
                 IO.puts("Tom is not exiting")
                 listen_jerry1()
           end
      end
   end
end   

Process.flag(:trap_exit, true)
count = String.to_integer(hd(System.argv()))
#customer_count = String.to_integer(hd(tl(System.argv())))

tom_pid = TomJerry.new_Tom()
jerry_pid = TomJerry.new_Jerry()
send(tom_pid, {:go, self()})

receive do
   {:EXIT, reason} ->
     IO.puts("A tom or jerry has exited (#{reason}).")
end
receive do
   {:EXIT, reason} ->
     IO.puts("A tom or jerry has exited (#{reason}).")
end
