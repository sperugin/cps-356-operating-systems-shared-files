# main does wait

# does not use registered Actor names; sender passed in each message

# count not passed on each recursive call; count passed in each message

defmodule PingPong do

   def new_ping() do
      spawn_link(__MODULE__, :listenPing, [])
   end

   def new_pong() do
      spawn_link(__MODULE__, :listenPong, [])
   end

   def listenPing() do
      receive do
         {:ping, sender, count} ->
            IO.puts("ping (#{count})")
            send(sender, {:pong, self(), count-1})
         {:shutdown} -> exit(:normal)
      end
      listenPing()
   end

   def listenPong() do
      receive do
         {:pong, sender, count} ->
            IO.puts("pong (#{count})")
            cond do
               count > 1 ->
                  send(sender, {:ping, self(), count-1})
               count == 1 ->
                  send(sender, {:shutdown})
                  exit(:normal)
            end
      end
      listenPong()
   end
end

i = String.to_integer(hd(System.argv()))

Process.flag(:trap_exit, true)
ping_pid = PingPong.new_ping()
pong_pid = PingPong.new_pong()
send(ping_pid, {:ping, pong_pid, i})

receive do
   {:EXIT, ^ping_pid, reason} ->
      IO.puts("ping has exited (#{reason})")
end

receive do
   {:EXIT, ^pong_pid, reason} ->
      IO.puts("pong has exited (#{reason})")
end
