import :timer, only: [ sleep: 1 ]

defmodule SleepingBarber do

   def new_barber() do
     pid = spawn_link(__MODULE__, :listen_barber, [true])
     Process.register(pid, :barber_actor)
     pid
   end
 
   def new_waitingroom(initial_chairs) do
     pid = spawn_link(__MODULE__, :listen_waiting_room,
                      [initial_chairs, :queue.new()])
     Process.register(pid, :waitingroom_actor)
     pid
   end
 
   def new_customer(id) do
     pid = spawn_link(__MODULE__, :start_customer, [id])
     pid
   end
 
   def listen_barber(sleeping) do
      cond do
         sleeping -> IO.puts("Barber is sleeping.")
         true -> IO.puts("Barber is awake.")
      end

      receive do
         ...
      end
      listen_barber(sleeping)
   end

   def spawn_customers(n) do
      new_customer(n)
      if n > 1 do
         spawn_customers(n-1)
      end
   end

   def start_customer(id) do
      IO.puts ("Customer #{id} arrived at the waiting room.")
      send(:waitingroom_actor, {:new_customer, {self(), id}})
      listen_customer(id)
   end

   def listen_customer(id) do
      receive do
         ...
      end
      listen_customer(id)
   end

   def listen_waiting_room(chairs_available, roomqueue) do

      IO.puts ("Chairs available: #{chairs_available}")

      receive do
         ...
      end
      listen_waiting_room(chairs_available, roomqueue)
   end
   
   def wait_for_end(count) do
      cond do
         count == 0 -> :ok
         true ->
           receive do
             {:EXIT, _pid, reason} ->
               IO.puts("A customer has exited (#{reason}).")
               IO.puts("Only #{count - 1} customers left.")
               wait_for_end(count - 1)
           end
      end
   end
end

Process.flag(:trap_exit, true)
waiting_room_size = String.to_integer(hd(System.argv()))
customer_count = String.to_integer(hd(tl(System.argv())))

SleepingBarber.new_barber()
SleepingBarber.new_waitingroom(waiting_room_size)
SleepingBarber.spawn_customers(customer_count)
SleepingBarber.wait_for_end(customer_count)
