defmodule Pattern do

   def head([a|b]) do
      a
   end

   def tail([a|b]) do
      b
   end

   def listlen([]) do
      0
   end

   def listlen([a|b]) do
      1 + listlen(b)
   end
 
end
