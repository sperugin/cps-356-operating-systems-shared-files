# main does wait

# uses registered Actor names; sender not passed in each message

# count not passed on each recursive call; count passed in each message

import :timer, only: [ sleep: 1 ]

defmodule PingPong do

   def play(count) do
      send(:ping_actor, {:ping, count})
   end

   def new_ping() do
      pid = spawn_link(__MODULE__, :listenPing, [])
      Process.register(pid, :ping_actor)
      pid
   end

   def new_pong() do
      pid = spawn_link(__MODULE__, :listenPong, [])
      Process.register(pid, :pong_actor)
      pid
   end

   def listenPing() do
       receive do
          {:ping, c} ->
              IO.puts("ping")
              send(:pong_actor, {:pong, c-1})

          {:shutdown} -> exit(:normal)
       end
       listenPing()
   end

   def listenPong() do
       receive do
          {:pong, c} ->
              IO.puts("pong")
              cond do
                 c > 1 ->
                    send(:ping_actor, {:ping, c-1})
                 c == 1 ->
                    send(:ping_actor, {:shutdown})
                    sleep(3)
                    exit(:normal)
              end
       end
       listenPong()
   end
end

i = String.to_integer(hd(System.argv()))

Process.flag(:trap_exit, true)

# assume ping pid is 87
ping = PingPong.new_ping()
# assume pong pid is 88
pong = PingPong.new_pong()
#PingPong.new_ping()
#PingPong.new_pong()

PingPong.play(i)

receive do
    #{:EXIT, x, reason} ->
    {:EXIT, ^pong, reason} -> # same message as {:EXIT, 87, reason}
        IO.puts("ping has exited (#{reason})")
end

receive do
    #{:EXIT, butIputpinghere, reason} ->
    {:EXIT, ^ping, reason} -> # same message as {:EXIT, 88, reason}
        IO.puts("pong has exited (#{reason})")
end
