# q is FIFO queue

# q = :queue.new() creates a new queue; returns a queue q
# :queue.is_empty(q) returns a boolean
# q = :queue.in({a,b}, q) is a push operation; returns a new queue q
# q = :queue.drop(q) is a pop operation; returns the tail of the queue q
# {a,b} = :queue.get(q) is a top operation; returns the head of the queue q

# IMPORTANT: none of these functions alter/modify their queue argument

#Examples:

# roomqueue is FIFO queue

roomqueue = :queue.new() creates a new queue
roomqueue = :queue.in({customer, id}, roomqueue)
{customer, id} = :queue.get(roomqueue)
roomqueue = :queue.drop(roomqueue)
