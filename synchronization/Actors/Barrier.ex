defmodule Barrier do
  
  def start_barrier(capacity) do
    spawn_link(__MODULE__, :barrier_loop, [capacity, []])
  end
  
  def barrier_loop(remaining_count, waiting_at_barrier) do
    receive do
       ...
    end
  end
  
  def notify_waiters([]), do: nil
  def notify_waiters([waiter|waiting_at_barrier]) do
    ###########################################################################
    ...
    ###########################################################################
  end
  
  def await_barrier(barrier) do
    ###########################################################################
    ...
    ###########################################################################
    receive do
    ###########################################################################
    ...
    ###########################################################################
    end
  end
end

defmodule BarrierTest do

  def run_barrier_test(count) do
    barrier = Barrier.start_barrier(count)
    IO.puts("Starting all but one waiter now.")
    Enum.map(
      0..(count-2),
      fn(_) -> start_test_barrier(barrier) end
    )
    IO.puts("Starting final waiter now.")
    start_test_barrier(barrier)
  end
  
  def start_test_barrier(barrier) do
    spawn_link(__MODULE__, :test_barrier, [barrier])
  end
  
  def test_barrier(barrier) do
    Barrier.await_barrier(barrier)
    IO.puts("Waiter continuing on after broken barrier.")
  end
end
