# main does not wait

# does not use registered Actor names; sender passed in each message

# count passed on each recursive call; count not passed in each message

defmodule PingPong do

   def new_ping(c) do
      spawn_link(__MODULE__, :listenPing, [c])
   end

   def new_pong(c) do
      spawn_link(__MODULE__, :listenPong, [c])
   end

   def listenPing(count) do
    if count != 0 do
       receive do
          {:ping, sender} ->
              IO.puts("ping")
              send(sender, {:pong, self()})
       end
       listenPing(count-1)
    end
   end

   def listenPong(count) do
     if count !=0 do
       receive do
          {:pong, sender} ->
              IO.puts("pong")
              send(sender, {:ping, self()})
       end
       listenPong(count-1)
     end
   end
end

i = String.to_integer(hd(System.argv()))

pingpid = PingPong.new_ping(i)
pongpid = PingPong.new_pong(i)

send(pingpid, {:ping, pongpid})
