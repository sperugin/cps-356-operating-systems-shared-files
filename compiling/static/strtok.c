/* wordaveragebad.c: ref. [USP] Chapter 2, Program 2.3, p. 39 */

#include <stdio.h>
#include <string.h>
#define WORD_DELIMITERS " \t\n"


main() {

   char line[] = "My  		 dear this is    surely a long\nline isn't it Sure jerry Don't forget your sunblock.";

   char* ptr = NULL;

   printf (":%s:\n\n", line);

   if (strtok(line, WORD_DELIMITERS) == NULL)
      return 1;
   else while ((ptr = strtok(NULL, WORD_DELIMITERS)) != NULL)
      printf (":%s:\n", ptr);

   return 0;
}
