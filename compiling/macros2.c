
/* max of two ints */
#define MAX(a,b) ((a) > (b) ? (a) : (b))

/* swap macro */
#define swap(x, y) int t = (x); (x) = (y); (y) = t;

/* swap function (call-by-value) */
void swap_cbv (int x, int y) {
   int temp = x;
   x = y;
   y = temp;
}

/* swap function (call-by-reference) */
void swap_cbr (int* x, int* y) {
   int temp = *x;
   *x = *y;
   *y = temp;
}

main() {

   int a = 1;
   int b = 2;
   /* int t = 5; */

   printf ("The max of %d and %d is %d.\n", 1, 2, MAX(1,2));
   printf ("The max of %d and %d is %d.\n", a, b, MAX(a,b));
   printf ("The max of %d and %d is %d.\n", b, a, MAX(b,a));
   printf ("The max of %d and %d is %d.\n", a+1, b+1, MAX(++a,++b));
   a--; b--;
   printf ("The max of %d and %d is %d.\n", a+1, b, MAX(a++,b));

   printf ("\n\n");

   printf ("before call-by-value: a = %d, b = %d\n\n", a, b);
   swap_cbv (a, b);
   printf ("after call-by-value: a = %d, b = %d\n\n", a, b);

   printf ("\n\n");

   printf ("before call-by-reference: a = %d, b = %d\n\n", a, b);
   swap_cbr (&a, &b);
   printf ("after call-by-reference: a = %d, b = %d\n\n", a, b);

   printf ("before swap macro: a = %d, b = %d\n\n", a, b);
   swap(a, b)
   /* swap(a, t) */
   printf ("after swap macro: a = %d, b = %d\n", a, b);
}
