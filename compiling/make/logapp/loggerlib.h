/*******************************************************************************
/
/      filename:  log.h
/   description:  Header file for loglib.c
/
/        author:  John Cresencia
/      login id:  cps445-n1.15
/
/         class:  CPS 445
/    instructor:  Perugini
/    assignment:  Homework #1
/
/      assigned:  January 18, 2006
/           due:  January 25, 2006
/
/******************************************************************************/

#include <time.h>

typedef struct data_struct {
   time_t time;
   char* string;
} data_t;

int addmsg(data_t data);
void clearlog(void);
char* getlog(void);
int savelog(char* filename);


