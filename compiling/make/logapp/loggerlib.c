#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "loggerlib.h"
 
typedef struct list_struct {
     data_t item;
     struct list_struct* next;
} log_t;
 
static log_t* headptr = NULL;
static log_t* tailptr = NULL;
 
int addmsg (data_t data) {
   log_t* newptr = malloc (sizeof (*newptr));

   if (newptr == NULL) {
      errno = ENOMEM;
      return -1;
   }

   newptr->next = NULL;

   newptr->item.string = malloc (strlen (data.string)+1);

   if (newptr->item.string == NULL) {
      errno = ENOMEM;
      free (newptr);
      return -1;
   }

   strcpy (newptr->item.string, data.string);

   newptr->item.time = data.time;

   if (headptr == NULL) {
      headptr = newptr;
      tailptr = newptr;
   } else {
        tailptr->next = newptr;
        tailptr = newptr;
     }

   return 0;
}

void clearlog (void) { 
   log_t* tptr1;
   log_t* tptr2;

   tptr1 = headptr;

   while (tptr1 != NULL) {
      tptr2 = tptr1->next;
      free (tptr1->item.string);
      free (tptr1);
      tptr1 = tptr2;
   }

   headptr = NULL;
   tailptr = NULL; 
} 

char* getlog (void) {
   int logsize = 0;

   log_t* tptr;

   char* allmsg;
   char* strpos;
   struct tm* loct;

   tptr = headptr;

   while (tptr != NULL) {
      logsize += strlen("Time: ");
      logsize += 18; /* Timestamp length */
      logsize += strlen("Message: ");
      logsize += strlen (tptr->item.string);
      logsize += strlen("\n\n");
      tptr = tptr->next;
   }

   allmsg = malloc (logsize+1);

   if (allmsg == NULL) {
      errno = ENOMEM;
      return NULL;
   }
   strpos = allmsg;
   tptr = headptr;

   while (tptr != NULL) {
      strcpy (strpos, "Time: ");
      strpos += strlen ("Time: ");
      loct = localtime(&(tptr->item.time));
      strftime (strpos, 19, "%x %X\n", loct);
      strpos += 18;
      strcpy (strpos, "Message: ");
      strpos += strlen ("Message: ");
      strcpy (strpos, tptr->item.string);
      strpos += strlen (tptr->item.string);
      strcpy (strpos, "\n\n");
      strpos += strlen ("\n\n");
      tptr = tptr->next;
   }

   *strpos = 0;

   return allmsg;
}
 
int savelog (char* filename) {
   log_t* tptr; 
   char datetime[19];
   char* logline;
   struct tm* loct;
 
   FILE* fout = fopen (filename, "w");

   if(fout == NULL) {
      errno = EIO;
      return -1;
   }
   
   tptr = headptr;
   while (tptr != NULL) {
      loct = localtime (&(tptr->item.time));
      strftime (datetime, 19, "%x %X\n", loct);
      logline = malloc (strlen (tptr->item.string)+strlen("Time: Message: ")+18+1);

      if (logline == NULL) {
         errno = ENOMEM;
         return -1;
      }

      strcpy(logline, "Time: ");
      strcat(logline, datetime);
      strcat(logline, "Message: ");
      strcat(logline, tptr->item.string);

      if (fprintf (fout, "%s\n\n", logline) < 0) {
         errno = EIO;
         free (logline);
         fclose (fout);
         return -1;
      }

      free (logline);
      tptr = tptr->next;
   }

   fclose (fout);   
   return 0;
}
