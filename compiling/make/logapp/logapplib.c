#include <stdio.h>
#include <stdlib.h>
#include "loggerlib.h"

/* execute cmd; store time and cmd in history list */
int execmd (char* cmd) {
   data_t execute;

   if (time (&(execute.time)) == -1)
      return -1;
   execute.string = cmd;

   /* command could not be executed at all */
   if (system (cmd) == -1)
      return -1;
   return addmsg (execute);
}

/* write the history list to stdout */
void displayhist (void) {
   char* logstring = getlog();

   printf ("%s\n", logstring);
   free (logstring);
}

/* write the history list to file with name file */
void savehistory (char* filename) {
   savelog (filename);
}

/*  clears the history list */
void clearhistory (void) {
   clearlog();
}
