#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define HISTFILE "history.log"

#ifndef MAX_CANON
#define MAX_CANON 8192
#endif   

int execmd (char* cmd);
void displayhist (void);

int main (int argc, char** argv) {
   char cmd[MAX_CANON];
   int history = 1;

   if (argc == 1)
      history = 0;
   else if ((argc > 2) || strcmp (argv[1], "history")) {
      fprintf (stderr, "Usage: %s [history]\n", argv[0]);
      return 1; 
   }   
   while (fgets (cmd, MAX_CANON, stdin) != NULL) {
      if (*(cmd + strlen(cmd) - 1) == '\n')
          *(cmd + strlen(cmd) - 1) = 0;
      if (history && !strcmp (cmd, "history"))
         displayhist();
      else if (execmd (cmd)) {
         perror ("Failed to execute command");
         break;
      }
   }  

   printf ("\n\n~~~~~~The following is the list of commands ordered by the ");
   printf ("time at which each was executed\n");

   displayhist();

   savehistory (HISTFILE);

   clearhistory();

   return 0; 
}
