#define SQUARE(X) ((X)*(X))

#define ONE 1

#define PRINT(A, B) printf(#A ": %d, " #B ": %d\n", A, B)

/* max of two ints */
#define MAX(a,b) ((a) > (b) ? (a) : (b))

#include<stdio.h>

main() {
   int a = 1;
   int b = 2;

   int x = SQUARE(3);
   int y = SQUARE(x+1);
   PRINT(x, y);

   printf ("\n\n");
   printf ("The max of %d and %d is %d.\n", 1, 2, MAX(1,2));
   printf ("The max of %d and %d is %d.\n", a, b, MAX(a,b));
   printf ("The max of %d and %d is %d.\n", b, a, MAX(b,a));
   printf ("The max of %d and %d is %d.\n", a+1, b+1, MAX(++a,++b));
   a--; b--;
   printf ("The max of %d and %d is %d.\n", a+1, b, MAX(a++,b));

}


/*

#A in a replacement string of a macro
   1. replace by an actual parameter
   2. enclose it in quotes

# expanded source code

main() {
   int x = ((3)*(3));
   int y = ((x+1)*(x+1));
   printf("x" ": %d, " "y" ": %d\n", x, y);

 */
