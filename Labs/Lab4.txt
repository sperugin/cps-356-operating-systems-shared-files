Lab 4: Exploring time-sharing OSs and threads.

Resources an Getting Help: 

   Threads & Thread-safe Functions Course Notes:
      http://perugini.cps.udayton.edu/teaching/courses/cps346/lecture_notes/threads.html

   (Shell) Job Control Course Notes:
      http://perugini.cps.udayton.edu/teaching/courses/cps346/lecture_notes/jobcontrol.html

    Linux Manpages (e.g., $ man htop)

Steps:

*) ssh into your Raspberry PI with the -X option on to ssh in Putty (under the 
   SSH X11 menu).

$ ssh -X pi@131....

* Copy some files from the share directory to your Raspberry Pi account:

$ scp SP_18_CPS356_nn@cpssuse04.cps.udayton.edu:/home/perugini_cps356/share/threads/counters.tar .

* Untar the files on your Raspberry Pi:

$ tar xvf counters.tar

$ cd Counter0

* Compile the Counter0 program:

$ javac *.java

* Run the Counter0 program:

$ java CounterGo0 &

* Create a new counter.  Can you create a second counter?
Can you exit the program via the GUI?  Why not?  Explain
why using operating systems nomenclature?  Is the counter0
program single-threaded or multi-threaded? Explain with reasons.

* Run the htop command to investigate the running Java
processes and threads in the system.

$ htop

Hit F-5 and drill into the process tree on the right hand side to
locate the java process.

$ Since you cannot exit the process normally, find the pid 
of the Java process using "ps" and then kill the process
with the kill command.

$ kill <pid>

* Now compile and run the Counter1 program using javac and java,
respectively, as above.  Create several counters?  What do you notice?  Are
they all running at the same time?  Do they appear to be all running at the
same time?  Is the Linux raspberrypi OS time-shared?  How would the counters
react if they were run on a non-time-shared system?

* Run the htop command to investigate the running Java
process and threads in the system.  What happens in
htop every time you spawn the new counter thread.

$ htop

Hit F-5 and drill into the process tree on the right hand side to
locate the java process.

* How many counters can you spawn before the CPU grinds to a halt?
Try to bring the system down.

* Run firefox

$ firefox&

* Run htop

$ htop

Hit F-5 and drill into the process tree on the right hand side to locate
Firefox.

* Run the htop command to investigate the running Firefox
process and threads in the system.  What happens in htop
every time you open a new tab in Firefox?
