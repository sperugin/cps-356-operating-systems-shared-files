Lab 2:

Resources an Getting Help: 

    [LP] Chapter 4: Introduction to C Programming: System Libraries and I/O

    System Libraries and I/O Course Notes:
       http://perugini.cps.udayton.edu/teaching/books/SPUC/www/lecture_notes/librariesIO.html

    Linux Manpages (e.g., $ man -s 2 fork)

Steps:

0) Work on these exercises locally on your Raspberry Pi, not on the SUSE 
systems.

Practice with int argc, char** argv on your Raspberry Pi.

1) Write a 10 line program called echoargs.c to print the value of argc, and
each individual element of argv to stderr, one per line. It is okay
to use array [] notation to do this.  Your output should match the
output of /home/perugini_cps356/share/librariesIO/echoargs.

For instance:

$ ./echoargs one two three four
argc is 5
argv[0] is ./echoargs
argv[1] is one
argv[2] is two
argv[3] is three
argv[4] is four

2) Copy echoargs.c to echopargs.c.  Modify the program so that it does not use
the array bracket notation [ ] anywhere in the program.  Rather use pointer
manipulation and pointer arithmetic.  Your output should match the output of
/home/perugini_cps356/share/librariesIO/echopargs.

For instance:

$ ./echopargs one two three four
argc is 5
Next argument is ./echopargs
Next argument is one
Next argument is two
Next argument is three
Next argument is four

3) Write a 15 line program called echocharargs.c to print the value of argc,
and each individual character of argv to stderr, one per line. It is okay to
use array [] notation to do this.  Your output should match the output of
/home/perugini_cps356/share/librariesIO/echocharargs.

For instance:

$ ./echocharargs one two three
argc is 4
argv[0][0] is .
argv[0][1] is /
argv[0][2] is e
argv[0][3] is c
argv[0][4] is h
argv[0][5] is o
argv[0][6] is c
argv[0][7] is h
argv[0][8] is a
argv[0][9] is r
argv[0][10] is a
argv[0][11] is r
argv[0][12] is g
argv[0][13] is s
argv[1][0] is o
argv[1][1] is n
argv[1][2] is e
argv[2][0] is t
argv[2][1] is w
argv[2][2] is o
argv[3][0] is t
argv[3][1] is h
argv[3][2] is r
argv[3][3] is e
argv[3][4] is e

4) Review the step-by-step Raspberry Pi instructions at (they have changed a 
bit since Lab 1):

http://perugini.cps.udayton.edu/teaching/books/SPUC/www/RaspberryPi/

Complete any parts you have not already completed.

5) Practice using scp (secure copy) to securely copy files from your
Raspberry Pi to your SUSE account and vice versa:

For instance:

(from your pi account to your SUSE account)
$ scp echoargs.c SP_18_CPS356_07@cpssuse04.cps.udayton.edu:

Login into your SUSE account to make sure the file echoargs.c
is now in your home directory.

(from your SUSE account to your pi account)
$ scp SP_18_CPS356_07@cpssuse04.cps.udayton.edu:echoargs.c copyofechoargs.c

Login into your pi account to make sure the file copyofechoargs.c
is now in your home directory.

Homework #2 due on Wednesday 2/7.
