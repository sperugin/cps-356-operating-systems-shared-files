import java.io.*;

class Read {
  public static void main(String[] args) {

    String line = null;
    int val = 0;

    try {
       BufferedReader is = new BufferedReader(new InputStreamReader(System.in));

       line = is.readLine();
       val = Integer.parseInt(line);

    } catch (NumberFormatException ex) {
         System.err.println("Not a valid number: " + line);
      } catch (IOException e) {
           System.err.println("Unexpected IO ERROR: " + e);
        }
    System.err.println("I read this number: " + val);
  }
}
