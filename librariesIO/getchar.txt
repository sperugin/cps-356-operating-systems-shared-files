Hi Dr. Perugini,

While I was working on the implementation of wc for homework 2, I decided to
run $ ./a.out a.out just for fun, but it gave me wrong output (in terms of byte
count, since "word count" and "newline count" for binary files are meaningless
anyway). Every ASCII file I tested produced correct output, and the
(simplified) code for counting bytes was essentially this:

for (bytes = 0; (c = fgetc(fp)) != EOF; bytes++);

It's hard to go wrong with one line of code, in fact I actually commented out
pretty much everything but this, and still couldn't figure out what went wrong.
Hours of man-page-reading and bug-hunting later, I found the problem: implicit
type conversion in C. Here's a rundown:

1. EOF is #defined as -1 (simple gcc -E check)
2. fgetc() returns an int
3. I declared c as a char (it makes more sense semantically)
4. ASCII only goes up to 0x7F
5. Binary files may contain 0x80 through 0xFF
6. In particular, when fgetc hits the first 0xFF, it returns 255, which gets converted into a char to be stored in c, then c gets converted back into an int to be compared with -1.

7. printf("%d", (int)(char)255) outputs -1 because char is apparently signed
char (which means the second half of all possible chars, i.e. non-ASCII, are
all negative), so the double conversion altered the original value, causing the
loop to terminate prematurely (at 255) before reaching the real EOF (at -1).

8. Just to confirm the previous point, printf("%d", (int)(unsigned char)196)
outputs 196, and printf("%d", 'a' > (char)207) outputs 1.

Of course, declaring c as an int fixed everything. But now I know I'll never
again assign fgetc() to a char.

********************

Unrelated question: Are we allowed to use stat() to determine the number of
bytes? Since the number of bytes is always the largest of the three counts, and
the field widths of all three counts are always equal, we actually need to find
the size of the largest file before we can even print anything. We can't just
save the entire 2-dimensional array of data, find the largest file size among
them, then print it all at once at the end, because that's not how wc behaves:
$ wc file1 - file2 prints the counts for file1, waits for user input and
Ctrl-D, then prints the counts for stdin and file2, so it actually prints the
counts for file1 before having any data about stdin. fseek() is not a very
efficient option because we need to open all the files to get their sizes, then
run through them again to get their counts. It looks like stat() is the only
reasonable option (or at least I can't find anything else on the internet or in
the man pages) to retrieve file sizes using only their paths or file
descriptors (without having to open them).

Thank you (and I really do apologize for the long email).
