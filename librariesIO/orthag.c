/* author: Perugini */

#include <stdio.h>
#include <string.h>
#ifndef DEBUG
#define DEBUG 0
#endif

main() {

   //char* x;
   //char* y;
   char* p;
   char* q;

   //x = "Hello";
   //y = "Si";

   char x[6], y[3];
 
   x[0]='H';
   x[1]='e';
   x[2]='l';
   x[3]='l';
   x[4]='o';
   x[5]='\0';
   y[0]='S';
   y[1]='i';
   y[2]='\0';

   printf ("\nx: [%s] of length %d\n", x, strlen(x));
   printf ("y: [%s] of length %d\n", y, strlen(y));

   p = x;
   q = y;

#ifdef DEBUG
p[0]='X';
printf("here\n");
#endif

   while ( *p++ = *q++ );
#ifdef DEBUG
printf("here\n");
#endif

   printf ("new x: [%s] of length %d\n", x, strlen(x));
   printf ("new y: [%s] of length %d\n", y, strlen(y));
}
