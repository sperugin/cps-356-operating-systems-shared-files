#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int main (int argc, char** argv) {

   int extern errno;

   FILE* basef;
   FILE* expf;
   FILE* outf;

   int base;
   int exp;
   int ans;
   int i;
   int rtn_val = 0;

   if (argc != 4) {
      fprintf (stderr, "Usage: %s basefile expfile outfile\n", argv[0]);
      exit (1);
   }

   basef = fopen (argv[1], "r");

   if (basef == NULL) {
      //fprintf (stderr, "Could not open base file %s: %s\n", argv[1],
       //        strerror(errno));
      perror ("Could not open base file");
      exit (1);
   }

   expf = fopen (argv[2], "r");

   if (expf == NULL) {
      fprintf (stderr, "Could not open exponent file: %s\n", argv[2]);
      exit (2);
   }

   outf = fopen (argv[3], "w");

   if (outf == NULL) {
      fprintf (stderr, "Could not open output file: %s\n", argv[3]);
      exit (1);
   }

   if ( (rtn_val = fscanf (basef, "%d", &base)) != 1) {
      fprintf (stderr, "Could not read decimal: %s\n", argv[1]);
      exit (1);
   }
   
   if ( (rtn_val = fscanf (expf, "%d", &exp)) != 1) {
      fprintf (stderr, "Could not read decimal: %s\n", argv[2]);
      exit (2);
   }

   ans = 1;

   for (i=0; i<exp; i++)
      ans *= base;

   if ( (rtn_val = fprintf (outf, "%d", ans)) == 0) {
      fprintf (stderr, "Could not write decimal to: %s\n", argv[3]);
      exit(3);
   }

   fclose(basef);
   fclose(expf);
   fclose(outf);

   exit(0);
}
