/* author: Perugini */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#ifndef MAX_CANON
/* #define LINELEN 256 */
#define MAX_CANON 8192
#endif

/* traverse.c */
int main() {

   char line[MAX_CANON+1];

   fgets(line, (MAX_CANON), stdin);

   printf (":%s:", line);
}
