import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
    
public class CounterGUI extends JFrame implements ActionListener
    {
    private int threadCount;
    private Vector counters;
    public CounterGUI()
        {
        threadCount=0;
        counters=new Vector();
        Container rootPane=getContentPane();
        rootPane.setLayout(new FlowLayout());
        JButton newBtn=new JButton("New Counter");
        newBtn.addActionListener(this);
        JButton exitBtn=new JButton("Exit");
        exitBtn.addActionListener(this);
        JButton reverseBtn=new JButton("Reverse");
        reverseBtn.addActionListener(this);
        rootPane.add(newBtn);
        rootPane.add(exitBtn);
        rootPane.add(reverseBtn);
        setLocation(200,200);
        pack();
        show();
        }
    public void actionPerformed(ActionEvent evt)
        {
        String cmd=evt.getActionCommand();
        if (cmd.equals("New Counter"))
            {
            threadCount=threadCount+1;
            Counter c1=new Counter(10000,"Counter #"+threadCount);
            counters.add(c1);
            Thread t1=new Thread(c1);
            t1.start();
            }
        else if (cmd.equals("Reverse"))
            {
            boolean notDone=true;
            int selected=0;
            Counter c1=null;
            while (notDone) 
            try
                {
                String ans=JOptionPane.showInputDialog(null,"Which counter?");
                if (ans==null) return;
                selected=Integer.parseInt(ans)-1;
                c1=(Counter)counters.get(selected);
                notDone=false;
                }
            catch (Exception ex) {}
            c1.changeDirection();
            }
        else if (cmd.equals("Exit"))
            System.exit(0);
        }
    }
