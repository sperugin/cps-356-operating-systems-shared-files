import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
    
public class Counter
    {
	private int x;
	private boolean countDown;
	private int maxCount;
	private String counterName;

	public Counter(int maximum,String name)
	    {
	    counterName=name;
	    countDown=true;
	    maxCount=maximum;
		x = maxCount;
	    }
	    
	public void count()
	    {
	    if (countDown)
    	    x=(x<=0)?maxCount:--x;
    	else
    	    x=(x>=maxCount)?0:++x;
	    }
	    
	public void changeDirection()
	    {
	    countDown=!countDown;
	    }
	    
	public void run()
	    {
	    JFrame rootFrame=new JFrame(counterName);
	    Container rootWin=rootFrame.getContentPane();
	    rootWin.setLayout(new FlowLayout());
	    JLabel lab=new JLabel(""+x);
	    lab.setFont(new Font("Helvetica",Font.BOLD,64));
	    rootWin.add(lab);
	    rootFrame.pack();
	    rootFrame.show();
	    while (1 != 2)
	        {
	        rootFrame.update(rootFrame.getGraphics());
	        lab.setText(""+x);
	        try {Thread.sleep(5);}
	        catch(Exception ex){System.exit(0);};
	        count();
	        }
	    }
    }
