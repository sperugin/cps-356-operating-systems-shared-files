import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
    
// Simple GUI to create multiple counters (Incorrect version)
public class CounterGUI extends JFrame implements ActionListener
    {
    private int threadCount;
    public CounterGUI()
        {
        threadCount=0;
        Container rootPane=getContentPane();
        rootPane.setLayout(new FlowLayout());
        JButton newBtn=new JButton("New Counter");
        newBtn.addActionListener(this);
        JButton exitBtn=new JButton("Exit");
        exitBtn.addActionListener(this);
        rootPane.add(newBtn);
        rootPane.add(exitBtn);
        setLocation(200,200);
        pack();
        show();
        }
    public void actionPerformed(ActionEvent evt)
        {
        String cmd=evt.getActionCommand();
        if (cmd.equals("New Counter"))
            {
            threadCount=threadCount+1;
            Counter c1=new Counter(10000,"Counter #"+threadCount);
            c1.run();
            }
        else if (cmd.equals("Exit"))
            System.exit(0);
        }
    }
