/* author: Perugini */

class ThrEx implements Runnable {

    public void run() {
       System.err.println("I am a thread,");
       System.err.println("and am still running,");
       System.err.println("and still running.");
    }

    public static void main(String args[]) {
       ThrEx objoftypeRunnable = new ThrEx();

       System.err.println("Thread " +
          (new Thread(objoftypeRunnable)).getId() + ": ");

       (new Thread(new ThrEx())).start();
       (new Thread(new ThrEx())).start();
       (new Thread(new ThrEx())).start();
       (new Thread(new ThrEx())).start();
       (new Thread(new ThrEx())).start();
    }
}
