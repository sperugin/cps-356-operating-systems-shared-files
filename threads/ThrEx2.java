/* author: Perugini */

public class ThrEx2 implements Runnable {

    public void run() {
       System.err.println("Thread " + Thread.currentThread().getId() + ": ");
       System.err.println("I am a thread,");
       System.err.println("and am still running,");
       System.err.println("and still running.");
    }

    public static void main(String args[]) {
       Thread t = null;

       Runnable objoftypeRunnable = new ThrEx2();

       System.err.println("Main Thread " +
          Thread.currentThread().getId() + ": ");

       System.err.println("Thread " +
          (new Thread(objoftypeRunnable)).getId() + ": ");

       for (int i=0; i<5; i++) {
          t = new Thread(new ThrEx2());
//          try { t.sleep(2); } catch(Exception e) { };

          //System.err.println("Thread " + t.getId() + ": ");
          t.start();
       }
    }
}

/*
1) create a class that implements Runnable (must define a run() method)

2) instantiate an object of type Runnable

3) instantiate a Thread object (call the Thread constructor)

4) pass to Thread constructor object instantiate in step 2
*/
