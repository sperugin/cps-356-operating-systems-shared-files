/* environ.c: ref. [USP] Chapter 2, Example 2.22, p. 49 */

/* outputs the contents of its environment list */
#include<stdio.h>

extern char** environ;

int main(int argc, char** argv) {

   int i;

   printf ("The environment list follows:\n");

   if (environ != NULL)
//      for (i=0; environ[i] != NULL; i++)
 //        printf ("environ[%d]: %s\n", i, environ[i]);
      while (*environ)
         printf ("environ[%d]: %s\n", i++, *environ++);

   return 0;
}
