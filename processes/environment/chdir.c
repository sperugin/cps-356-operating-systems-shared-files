/* author: Perugini */

#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>
#include<limits.h>

#ifndef MAX_CANON
#define MAX_CANON 8192
#endif

// extern char** environ;

main() {
   FILE* ptr = NULL;
   int x;


   char* mycwd = malloc(MAX_CANON);

   getcwd(mycwd, MAX_CANON);
   fprintf(stderr, "CWD = :%s:\n", mycwd);

   fprintf(stderr, "PWD = :%s:\n", getenv("PWD"));

   setenv("PWD", "/home/perugini_cps346", 1);

   fprintf(stderr, "Just set PWD to /home/perugini_cps346\n");

   fprintf(stderr, "PWD = :%s:\n", getenv("PWD"));

   ptr = fopen ("num.txt", "r");
   fscanf (ptr, "%d", &x);
   fprintf (stderr, "%d", x);

   setenv("PWD", "/home", 1);
   fprintf(stderr, "Just set PWD to /home\n");

   chdir("/home/perugini_cps346/");
   //chdir("../../../");
   fprintf(stderr, "Just cd'd to /home/perugini_cps346\n");

   fprintf (stderr, "PWD = :%s:\n", getenv("PWD"));

   fprintf(stderr, "CWD = :%s:\n", mycwd);

   ptr = fopen ("num.txt", "r");
   fscanf (ptr, "%d", &x);
   fprintf (stderr, "%d\n", x);

   fprintf(stderr, "PWD = :%s:\n", getenv("PWD"));
}
