#include <stdio.h>
#include <stdlib.h>
int main() {

   int x = 5;

   int* x_ptr = &x;

   fprintf(stderr, "x = %d, &x = %X\n", x, x_ptr);
 
   int child_pid = fork();

   wait(NULL);
 
   if (child_pid == 0){
      fprintf(stderr, "\nI am the child, changing value at %X\n", x_ptr);
      *x_ptr = 10;
      fprintf(stderr, "Now the value at %X is %d\n", x_ptr, *x_ptr);
   }
   else{
      fprintf(stderr, "\nI am the parent, value at %X is %d\n", x_ptr, *x_ptr);
   }
   exit(0);
}
