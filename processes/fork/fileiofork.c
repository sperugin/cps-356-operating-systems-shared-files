/* fileiofork.c: ref. [USP] Chapter 4, Exercise 4.33, p. 128 */

#include <stdio.h>
#include <unistd.h>

main() {
   int x;
   //fprintf(stdout, "Print this.");

   fprintf(stdout, "Print this.\n");
   fprintf(stdout, "Print this.\n");
   //fprintf(stderr, "Print this.");
   //fflush(stdout);
   //scanf("%d", &x);
   fclose(stdout);

   fork();

   printf("output\n");

   //fclose(stdout);
   //close(STDOUT_FILENO);
   //close(1);
}
