/* twoprocs.c: ref. [USP] Chapter 3, Example 3.6, p. 65 */

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

int main() {

   pid_t childpid;

   childpid = fork();
   if (childpid == -1) {
      perror("Failed to fork");
      return 1;
   }
   if (childpid) {                             /* parent code */
//      sleep(5);
      fprintf(stderr, "I am parent %ld\n", (long) getpid());
   } /* child code */
   else {
      sleep(5);
      fprintf(stderr, "I am child %ld\n", (long) getpid());
   }
   return 0;
}
