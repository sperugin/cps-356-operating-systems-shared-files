/* author: Perugini */

#include<stdio.h>
#include<stdlib.h>
#define SIZE 10

main() {

   int i;

   int* intarray = calloc (SIZE, sizeof (*intarray));

   for (i=0; i < SIZE; i++)
      intarray[i] = i+1;

   for (i=0; i < SIZE; i++)
      fprintf (stderr, "%d\n", intarray[i]);

   fprintf (stderr, "\n");

   intarray = realloc (intarray, sizeof (*intarray)*(SIZE+SIZE));

   for (i=SIZE; i < (SIZE+SIZE); i++)
      intarray[i] = i+1;

   for (i=0; i < (SIZE+SIZE); i++)
      fprintf (stderr, "%d\n", intarray[i]);
}
