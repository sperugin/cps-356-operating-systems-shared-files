/* author: Perugini */

#include<stdio.h>
#include<stdlib.h>

main() {
   int i;
   int* intarray;

   int size;

   scanf("%d", &size);

   //int intarray[size];

   /* preferred */
   intarray = malloc (sizeof (*intarray)*size);

   /* not recommended */
   //intarray = (int*) malloc (sizeof (int)*size);

   //int intarray[size];

   for (i=0; i < size; i++)
      intarray[i] = i+1;

   for (i=0; i < size; i++)
      printf ("%d\n", intarray[i]);

   //intarray = NULL;

   free(intarray);
}
