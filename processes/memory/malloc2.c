/* author: Perugini */

#include<stdio.h>
#include<stdlib.h>

#define DEBUG 

int* intarray;

/* function called at the time main exits to make sure memory is freed */
static void freemem(void) {
   #ifdef DEBUG
      fprintf(stderr, "Totally exiting!\n");
   #endif
   free(intarray);
}

main() {

   int i;

   int size;

   scanf("%d", &size);

   intarray = malloc (sizeof(*intarray)*size);

   //int intarray[size];

   if (atexit(freemem)) {
      fprintf(stderr, "Unable to install freemem error handler.\n");
   }

   for (i=0; i < size; i++)
      intarray[i] = i+1;

   for (i=0; i < size; i++)
      printf ("%d\n", intarray[i]);

   /* free (intarray); */
   exit(0);
}
